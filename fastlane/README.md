fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios deploy
```
fastlane ios deploy
```
Deploy ios app on the appstore
### ios build
```
fastlane ios build
```

### ios screenshots
```
fastlane ios screenshots
```


----

## Android
### android build
```
fastlane android build
```
Deploy android app on play store
### android update_meta
```
fastlane android update_meta
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
