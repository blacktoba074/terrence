# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.13.1](https://gitlab.com/karen/terrence/compare/v1.13.0...v1.13.1) (2021-02-03)


### Bug Fixes

* **build:** remove social cordova plugin ([4d572d7](https://gitlab.com/karen/terrence/commit/4d572d71ed535508ce594ee961951a0bcccebc65))

## [1.13.0](https://gitlab.com/karen/terrence/compare/v1.12.1...v1.13.0) (2021-02-03)


### Features

* **goal:** propose social sharing when the objective is reached ([d2bad00](https://gitlab.com/karen/terrence/commit/d2bad008f120eb55cbad7cadce922bdc6c892a15))

### [1.12.1](https://gitlab.com/karen/terrence/compare/v1.12.0...v1.12.1) (2021-01-29)


### Bug Fixes

* remove unused import ([649cf94](https://gitlab.com/karen/terrence/commit/649cf94d73d88b9fc5420f2d79dc1363eadb78ed))

## [1.12.0](https://gitlab.com/karen/terrence/compare/v1.11.0...v1.12.0) (2021-01-29)


### Features

* **newLevel:** bonus from reaching a level ([7453b90](https://gitlab.com/karen/terrence/commit/7453b905a8ae961ad3bfb1d8a5d8b7bc6042ec95))


### Bug Fixes

* **gamebalance:** misc changes ([a25ba33](https://gitlab.com/karen/terrence/commit/a25ba3340168790cf99834be109af2516daf2397))
* **impact:** fix left rule for right "not buy" ([a383dee](https://gitlab.com/karen/terrence/commit/a383dee52f136aecbc744fab8cb9619644685675))
* **swipe-tutorial:** display when first card is shown ([e1fda2e](https://gitlab.com/karen/terrence/commit/e1fda2e90cdddabeee2fecf3a70d963817cf28cb))

## [1.11.0](https://gitlab.com/karen/terrence/compare/v1.10.1...v1.11.0) (2021-01-27)


### Features

* **expansion:** tweet based on the current city condition ([3a6e585](https://gitlab.com/karen/terrence/commit/3a6e585e5a2437ad8e09970b98f64003fcfb9b27))


### Bug Fixes

* **intro:** first card should not generate a game over ([90663da](https://gitlab.com/karen/terrence/commit/90663dade76c0f3fe9ae76634a2c4a0d34eb184e))
* **intro:** first card, cash should be 2M ([356f473](https://gitlab.com/karen/terrence/commit/356f4737ec4d62669b3e8a38ded306258c45c0da)), closes [#54](https://gitlab.com/karen/terrence/issues/54)

### [1.10.1](https://gitlab.com/karen/terrence/compare/v1.10.0...v1.10.1) (2021-01-27)


### Bug Fixes

* **newTurn:** better style ([f68754a](https://gitlab.com/karen/terrence/commit/f68754a1b149b89c07aee9404e433bd6b4f8ed08))

## [1.10.0](https://gitlab.com/karen/terrence/compare/v1.9.1...v1.10.0) (2021-01-27)


### Features

* **newTurn:** show progress as a bar at each turn ([0439f5c](https://gitlab.com/karen/terrence/commit/0439f5c6bb61a1838c353793821a19ce9556d0a9))


### Bug Fixes

* make game a little snappier ([193703d](https://gitlab.com/karen/terrence/commit/193703d98038fc8ea021c2de5d71267569977172))

### [1.9.1](https://gitlab.com/karen/terrence/compare/v1.9.0...v1.9.1) (2021-01-22)


### Bug Fixes

* **animation:** use react.Memo on various components ([f2a32d7](https://gitlab.com/karen/terrence/commit/f2a32d7a4231fd44dd4cdf192cdf557f4d0a64e3))
* **lovewall:** remove opacity transition ([bf820e0](https://gitlab.com/karen/terrence/commit/bf820e0ca4638814c80b688019ba4963dacfc8cc))
* **timeline:** use sensible height value when ref is not available ([a9318d3](https://gitlab.com/karen/terrence/commit/a9318d385bacac33c890f7b3a02d0ac4e40bd706))
* **uistate:** move card selection in subsequent stack ([304e811](https://gitlab.com/karen/terrence/commit/304e81153eca72d598063ff4c8e6be41ecb910d7))

## [1.9.0](https://gitlab.com/karen/terrence/compare/v1.8.4...v1.9.0) (2021-01-20)


### Features

* **infra:** update maintenance prices ([62e8b72](https://gitlab.com/karen/terrence/commit/62e8b72cf681acdea1e322c6a7b9b96bd6e93044))


### Bug Fixes

* **flags:** prefer local storage over global variable for outside react ([71cebf1](https://gitlab.com/karen/terrence/commit/71cebf19eb20bdc233326cd201dc2cc76e27296b))
* **win:** when all services are bought, player wins ([300d776](https://gitlab.com/karen/terrence/commit/300d7762b4201ed90e25a8a47b160c96016f7cd0))

### [1.8.4](https://gitlab.com/karen/terrence/compare/v1.8.3...v1.8.4) (2021-01-19)


### Bug Fixes

* **tour:** mobile positioning on mobile ([d513aec](https://gitlab.com/karen/terrence/commit/d513aec778e0e3a24a061172be2a2ff7ec93c4c7))

### [1.8.3](https://gitlab.com/karen/terrence/compare/v1.8.2...v1.8.3) (2021-01-15)


### Bug Fixes

* **layout:** ref is on the content space ([ee1810e](https://gitlab.com/karen/terrence/commit/ee1810e3411bcb6b99acf4b04154277c264634eb))
* **tour:** more obvious reference to element ([bcc7be0](https://gitlab.com/karen/terrence/commit/bcc7be046b55dd7e43936fa990bc2481e6e45644))

### [1.8.2](https://gitlab.com/karen/terrence/compare/v1.8.1...v1.8.2) (2021-01-15)


### Bug Fixes

* **amplitude:** add user properties ([a910ec4](https://gitlab.com/karen/terrence/commit/a910ec431b529f5e28212289b5d90c9b23bd4e41))

### [1.8.1](https://gitlab.com/karen/terrence/compare/v1.8.0...v1.8.1) (2021-01-15)


### Bug Fixes

* **animation:** tweak shower bill ([a113385](https://gitlab.com/karen/terrence/commit/a1133855b76945ed41c050dacf6e5b181668e44c))
* **intro:** make the goal more central ([705ef1f](https://gitlab.com/karen/terrence/commit/705ef1feb403623d89868646566ffb914b7ddec1))
* **timeline:** flow of thread should follow reading order ([965d685](https://gitlab.com/karen/terrence/commit/965d685f42d823dbecbe3858b7841083612a13ad)), closes [#48](https://gitlab.com/karen/terrence/issues/48)
* **tour:** fix typos and rewording ([8db939e](https://gitlab.com/karen/terrence/commit/8db939ebef8e4209f14f44e435872bad09ea8ee8)), closes [#41](https://gitlab.com/karen/terrence/issues/41)

## [1.8.0](https://gitlab.com/karen/terrence/compare/v1.7.0...v1.8.0) (2021-01-13)


### Features

* tweets can be unrelated with suggestion ([7fe43e0](https://gitlab.com/karen/terrence/commit/7fe43e047e2938868003885a703303e22900adb7))


### Bug Fixes

* **contracts:** prevent threshold from last contract ([2862ebb](https://gitlab.com/karen/terrence/commit/2862ebb5e985a9bd7612db80a76fb1d48b3c6cb7))

## [1.7.0](https://gitlab.com/karen/terrence/compare/v1.6.1...v1.7.0) (2021-01-13)


### Features

* **tour:** implement a tour / tutorial using joyride ([b54141b](https://gitlab.com/karen/terrence/commit/b54141bf665702769e146ee580225dc294ce105b))
* **tutorial:** never show the tutorial again after once ([2f615da](https://gitlab.com/karen/terrence/commit/2f615da050e691d40173c896af21397474536392))


### Bug Fixes

* **tour:** style ([94f63fc](https://gitlab.com/karen/terrence/commit/94f63fcdca79bc5df7a3c36fe86afde767b1b623))
* **tutorial:** remove swipe arrow ([135ae52](https://gitlab.com/karen/terrence/commit/135ae5213f4cbd276d56d981c4d18579662f3c55))

### [1.6.1](https://gitlab.com/karen/terrence/compare/v1.6.0...v1.6.1) (2021-01-11)


### Bug Fixes

* **amplitude:** add context and version for analytics ([abe8f4e](https://gitlab.com/karen/terrence/commit/abe8f4ef10323b9777b4ab61eb497e64a9dd700e))
* **lovewall:** correct layer of the animation ([b7fccd9](https://gitlab.com/karen/terrence/commit/b7fccd9fb7452501f5372eba03c53f6230b6f2de))
* **swipe-tuto:** position arrows to make them readable ([27e505a](https://gitlab.com/karen/terrence/commit/27e505a3388559bf95a3510918ec74fe16bceabe))
* **timeline:** tweets should still be scrollable ([e88716c](https://gitlab.com/karen/terrence/commit/e88716cbcab18eb18d4407725df62cdb70a96ad8)), closes [#37](https://gitlab.com/karen/terrence/issues/37)

## [1.6.0](https://gitlab.com/karen/terrence/compare/v1.5.0...v1.6.0) (2021-01-08)


### Features

* **intro:** non punishing answers ([6d745fd](https://gitlab.com/karen/terrence/commit/6d745fd30bf9234abd02e0c99bf5337f05e5ef75))

## [1.5.0](https://gitlab.com/karen/terrence/compare/v1.4.3...v1.5.0) (2021-01-06)


### Features

* **art:** trophies for levels 7 and 8 ([99a70c1](https://gitlab.com/karen/terrence/commit/99a70c1e0ca488a0001ff4273fe3f5fc676d7a64))

### [1.4.3](https://gitlab.com/karen/terrence/compare/v1.4.2...v1.4.3) (2021-01-06)


### Bug Fixes

* **tutorial:** new wording ([a841abb](https://gitlab.com/karen/terrence/commit/a841abbf8052234a5fcb5b572e5ed047f762046a))

### [1.4.2](https://gitlab.com/karen/terrence/compare/v1.4.1...v1.4.2) (2021-01-06)


### Bug Fixes

* **game:** service level update when all the services are bought ([f80da20](https://gitlab.com/karen/terrence/commit/f80da204ed504f58afca218091efaba6bf5540b3))

### [1.4.1](https://gitlab.com/karen/terrence/compare/v1.4.0...v1.4.1) (2021-01-06)


### Bug Fixes

* **config:** remove unused code ([1de0a47](https://gitlab.com/karen/terrence/commit/1de0a47a275c4064fc095551576ae16f5918bb28))

## [1.4.0](https://gitlab.com/karen/terrence/compare/v1.3.0...v1.4.0) (2021-01-06)


### Features

* **foldable-line:** allow for render props ([223a571](https://gitlab.com/karen/terrence/commit/223a571834971e9338157cc7e3c8e2edb091b908))
* **levels:** completion badges and hidden cards ([19aafc8](https://gitlab.com/karen/terrence/commit/19aafc8c88f4740d36560f356489eeafb0b980fb))
* **services:** display by level ([5a718a2](https://gitlab.com/karen/terrence/commit/5a718a2a70fa0c5b5e1a0fe9ba109c43b265c49d))
* **stats:** services by levels ([d36004c](https://gitlab.com/karen/terrence/commit/d36004cceddd9949791e65df0614a05482b2344f))


### Bug Fixes

* **foldable-line:** style update ([49c2570](https://gitlab.com/karen/terrence/commit/49c25709ee95bb33f627ae54a017e886f048bf72))
* **stats:** option param ([387fedf](https://gitlab.com/karen/terrence/commit/387fedf0914bd28174173f3ce454d188cb145cf6))

## [1.3.0](https://gitlab.com/karen/terrence/compare/v1.2.3...v1.3.0) (2020-12-22)


### Features

* **contract:** new values for contracts ([633ff78](https://gitlab.com/karen/terrence/commit/633ff78e723a747e6fbfb6a944ee64e5a895416c))
* **plots:** config based expansion parameters ([3e8bb36](https://gitlab.com/karen/terrence/commit/3e8bb367fc291b9e5254e08d58141e8466b65dba))


### Bug Fixes

* **contract:** percentage typo ([41115b4](https://gitlab.com/karen/terrence/commit/41115b4586ed54e408adeeb6aafdcda4b910d377))
* **events:** typo ([fbacfd4](https://gitlab.com/karen/terrence/commit/fbacfd438c3d84af13a24d2d340c58a158ac2092))
* **gameover:** missing not owner losing condition ([ab850f9](https://gitlab.com/karen/terrence/commit/ab850f98fee25f512730fe643646ddd76d15b1a5))
* **impact:** streak bonus decrease as city grows ([d1f72d2](https://gitlab.com/karen/terrence/commit/d1f72d22a24dcd4f53a3b4b1aa3a14e9fc91227f))

### [1.2.3](https://gitlab.com/karen/terrence/compare/v1.2.2...v1.2.3) (2020-12-21)


### Bug Fixes

* **version:** display version in debug ([a847e1b](https://gitlab.com/karen/terrence/commit/a847e1b6b419978cf66ae125d5cfe8bcd35563e2)), closes [#30](https://gitlab.com/karen/terrence/issues/30)

### [1.2.2](https://gitlab.com/karen/terrence/compare/v1.2.1...v1.2.2) (2020-12-18)

### [1.2.1](https://gitlab.com/karen/terrence/compare/v1.2.0...v1.2.1) (2020-12-18)


### Bug Fixes

* **copy:** fix some typos ([5c12b8a](https://gitlab.com/karen/terrence/commit/5c12b8a5779d93800fd56728c5490617d16b33b7)), closes [#25](https://gitlab.com/karen/terrence/issues/25)

## 1.2.0 (2020-12-18)


### Features

* **art:** new events ([bee279d](https://gitlab.com/karen/terrence/commit/bee279d0c439bbcc365df4669b13179e06cba567))
* **design:** new buttons with automatic implementation ([faba87d](https://gitlab.com/karen/terrence/commit/faba87db296484e723fe5312a7da7a9cbd2a3fc8))
* **events:** update design ([977e1a1](https://gitlab.com/karen/terrence/commit/977e1a1c8df0ee81e4b6d908bd39de2d0266782d))
* **infra:** design update ([14ab0a6](https://gitlab.com/karen/terrence/commit/14ab0a6e6c0b2ce3ed6eabebda5a3e034d4dd8da))
* swipe tutorial ([7f96ade](https://gitlab.com/karen/terrence/commit/7f96ade7b810724bdcfa6feb669fdd950616cb08))
* **amplitude:** city level and goal reached events ([ccc2c12](https://gitlab.com/karen/terrence/commit/ccc2c127c8551a7e06d615872ca724896d9924a7))
* **amplitude:** new events and fix duplicate ([66a5fb6](https://gitlab.com/karen/terrence/commit/66a5fb636e87aca07dab5517d3927eb81c08d7af))
* **debug:** allow debug to crash the app ([b46da0f](https://gitlab.com/karen/terrence/commit/b46da0f1541d0558deb56eacdca9717832771926))
* **debug:** better default message for crash report ([ea6df13](https://gitlab.com/karen/terrence/commit/ea6df13c4de352cf2f5b47c9786dfe635e59daee))
* **debug:** feature flags automatic UI ([52c23fd](https://gitlab.com/karen/terrence/commit/52c23fd9844ecc09bb0fdd65dc6493340a733613))
* **events:** add illustration and modify pipeline ([83d719d](https://gitlab.com/karen/terrence/commit/83d719db9c826d90bf592b9154ee49a22d1b74d1))
* **events:** import from google sheet ([8e175ec](https://gitlab.com/karen/terrence/commit/8e175eccc29fc1bdc7c250de9e530545abd0b254))
* **events:** mechanics for events ([49cc8f5](https://gitlab.com/karen/terrence/commit/49cc8f5faddc1fbcb931a934f6fe0245240fe086))
* **footer:** value animations ([0e54d85](https://gitlab.com/karen/terrence/commit/0e54d85ee7c93af2903dd218d898b940fb767d68))
* **gamedesign:** start with 0$ ([3eb98c8](https://gitlab.com/karen/terrence/commit/3eb98c8b8ed2d86d142803823e1af7cd85a85113))
* **homepage:** dynamic links to app + new design ([ad5b030](https://gitlab.com/karen/terrence/commit/ad5b03035f0f5c011806864327b0cf0f6fa01929))
* **levels:** generate json file from spreadsheet ([f6dcf75](https://gitlab.com/karen/terrence/commit/f6dcf757f37dc7e36559b279411803e8d7daf662))
* **website:** add Google Play store badge ([87451eb](https://gitlab.com/karen/terrence/commit/87451ebdb275d297cf4336f7b87d4a4b964c1ec4))
* activate new service tree and events ([ab79a69](https://gitlab.com/karen/terrence/commit/ab79a69f0a593d9791997e4cdfb8586c41702682))
* add emoji when buying the good stuff ([3146fe6](https://gitlab.com/karen/terrence/commit/3146fe64687f1693382a5a303c5bd0db6f801611))
* add star icon ([f65e1c7](https://gitlab.com/karen/terrence/commit/f65e1c7a6d94e88bea668b6cf5f9388edaae0092))
* add storybook for component dev ([39ac481](https://gitlab.com/karen/terrence/commit/39ac481bd9a31aab1b48bb7789675ba655f71c01))
* contract update card ([954343b](https://gitlab.com/karen/terrence/commit/954343b232197e71b25850c5a059e604444a409a))
* display level animation when a new one is reached ([d31f6cb](https://gitlab.com/karen/terrence/commit/d31f6cb3f8550d1523ca005a9cabeb5a437ac6cd))
* **error:** reload game when crashed + popup ([84e1920](https://gitlab.com/karen/terrence/commit/84e1920508ce73888fab6f29104fe711a810b91c))
* **homepage:** logo for pwa like appstore ([b73bbba](https://gitlab.com/karen/terrence/commit/b73bbba064dd5b9749a190bb7165e91858841d15))
* **levels:** implement pick service ([a17befd](https://gitlab.com/karen/terrence/commit/a17befdf2499997ecec8f0551a552f1ce67815a5))
* add privacy policy ([838ab45](https://gitlab.com/karen/terrence/commit/838ab4581c229e05c9e20f900ccf5d067a6689fb))
* choose your city name ([6a4c10c](https://gitlab.com/karen/terrence/commit/6a4c10cf8a4468e7d61680403635cff4d7b2cb49))
* levels as tree ([e50c405](https://gitlab.com/karen/terrence/commit/e50c4051eb9e0f9a145e9f0b12167fb002a2a46c))
* The city name is randomly generated at each new game ([d60c56a](https://gitlab.com/karen/terrence/commit/d60c56ac040077d86d2eac66f31c33e925debbf2))
* update events ([22951a0](https://gitlab.com/karen/terrence/commit/22951a0d40e3807dd05678f8f984d70e8b50618d))
* **apps:** generate icons from single PNG ([f0adb2c](https://gitlab.com/karen/terrence/commit/f0adb2cda5b5f2baf69a10c0c013d94efb290a57))
* **art:** asset update with new contract illustration ([9693f70](https://gitlab.com/karen/terrence/commit/9693f7083ee6ec2212754cfe1995669bf9b61ede))
* **art:** new land owners avatars ([a5efdc8](https://gitlab.com/karen/terrence/commit/a5efdc8a026ddb868d09f7ea4e001a5369195193))
* **art:** update ([f014c6f](https://gitlab.com/karen/terrence/commit/f014c6fc434c904e31f86b7be715844ef43b0b9c))
* **art:** update assets ([5891abc](https://gitlab.com/karen/terrence/commit/5891abc9ab4eedf2e2f9312ac2f9f0b5a07a6cd1))
* **art:** update assets ([72da199](https://gitlab.com/karen/terrence/commit/72da19907701ab51cc353d6a4d442480ac5f2082))
* **art:** update assets ([a824c54](https://gitlab.com/karen/terrence/commit/a824c546bd9075f21e8afee23945bb8f71269b65))
* **art:** update colors and cards ([93ee42e](https://gitlab.com/karen/terrence/commit/93ee42e418cdd41b9deb235fd4841e783d20e1a2))
* **art:** update goal reached and win modals ([3d40850](https://gitlab.com/karen/terrence/commit/3d40850850772d8b190a16d8baee7485848e6073))
* **automation:** assets pipeline ([4a7659d](https://gitlab.com/karen/terrence/commit/4a7659d9f0c532bd8195b0eb7ea238420d34a151))
* **cards:** integration of assets WIP ([f9dcaf4](https://gitlab.com/karen/terrence/commit/f9dcaf4f77b50cdec69a5c0f90c2b9b414d1baa3))
* **content:** provide links to CCI and 2xgdp ([9a45f45](https://gitlab.com/karen/terrence/commit/9a45f45543a62cd298b31893f2bc26fdaeec25db))
* **debug:** config for display cards in stat ([bd9238f](https://gitlab.com/karen/terrence/commit/bd9238ff084bb7cb98aa416e400276753f687104))
* **design:** always use new design buttons ([339bd87](https://gitlab.com/karen/terrence/commit/339bd870438151db0494eed4eaee96ba696a8014))
* **design:** colors ([812dc8a](https://gitlab.com/karen/terrence/commit/812dc8a27b24ce04ad25f731cadfc1bb132c2d3d))
* **design:** integrate colors ([05c4ce2](https://gitlab.com/karen/terrence/commit/05c4ce2bafe8801e8d12394dfbde6514378030b1))
* **design:** integrate design oct 8th ([817de4c](https://gitlab.com/karen/terrence/commit/817de4c1791f4410fd0ae480cf382967d817ba6b))
* **design:** new title screen ([bf4995f](https://gitlab.com/karen/terrence/commit/bf4995f5360ff0237edc9a6fb115c3b9fd5710f7))
* **dev:** screenshot automation ([bd78b4b](https://gitlab.com/karen/terrence/commit/bd78b4b6ee7408bba9778b0ca8b591b88ee1c5b6))
* **fastlane:** delivery to itunes connect ([007b4ea](https://gitlab.com/karen/terrence/commit/007b4ea7fdc7791cb5820403228e71ee5184d85f))
* **footer:** display meters and update icons ([bb0abb3](https://gitlab.com/karen/terrence/commit/bb0abb3930bbcefad20a9505a5ab32828717197c))
* **footer:** display numbers and update icon for calendar ([88a2d94](https://gitlab.com/karen/terrence/commit/88a2d94451ef7663843b594b7d7b5f0fc45768d1))
* **footer:** full width ([e5d7f72](https://gitlab.com/karen/terrence/commit/e5d7f725ddd14620a41d69a7ff8b27fc0632ae64))
* **gameplay:** game balance ([59324b5](https://gitlab.com/karen/terrence/commit/59324b545e8e7d49cbd57aed26862563c4ff1ce0))
* **gameplay:** plot expansion with dynamic happiness impact ([2fe7c5d](https://gitlab.com/karen/terrence/commit/2fe7c5d3e41877984c6caa0cee589ef68f379a06))
* **homepage:** add links to discord and gitlab ([8232603](https://gitlab.com/karen/terrence/commit/82326031b3f3310ef5b87447277891fdcb215867))
* **homepage:** folding banner past the header ([25641f2](https://gitlab.com/karen/terrence/commit/25641f28d87c47997e6d699e3818315c99260e39))
* **homepage:** routing and fixes for homepage ([e1743d0](https://gitlab.com/karen/terrence/commit/e1743d002154d291f51ab1bb806b1047bf1134f2))
* **homepage:** social links + badges ([eb00765](https://gitlab.com/karen/terrence/commit/eb0076559645cd6fb2b492aaaf59e8787c46cdc3))
* **newturn:** animations + city name ([1af8567](https://gitlab.com/karen/terrence/commit/1af8567842b6d306de2281b312595b28673f5e00))
* **newTurn:** animation update ([34964b1](https://gitlab.com/karen/terrence/commit/34964b1cbda65a19cf3b73adbe7a09c3f4a95a36))
* **newTurn:** show bills rain when balance is increasing ([2f2959c](https://gitlab.com/karen/terrence/commit/2f2959c1b07d86be90ce003684690b034d61ff4a))
* **price:** price from config file ([e5b8972](https://gitlab.com/karen/terrence/commit/e5b89720773da7ffce30340c8ff8767a6f014987))
* **serviceworker:** update popup ([ab6e87d](https://gitlab.com/karen/terrence/commit/ab6e87deed9cbe602fcb89848e98e55cb9477261))
* **stats:** add last investment ([9bca394](https://gitlab.com/karen/terrence/commit/9bca394c0a9babd51a41204e9490a7b8359b6afe))
* **stats:** more info in finance screen ([96ff4c8](https://gitlab.com/karen/terrence/commit/96ff4c8835bd3d6d29eaa16777f402a03fc490c7))
* **stocks:** no stocks ([c8eebe2](https://gitlab.com/karen/terrence/commit/c8eebe2595699213b06f420cd608866c776ea409))
* **stories:** win modal stories ([b1caaa1](https://gitlab.com/karen/terrence/commit/b1caaa14eab9881d99c1d4f5b6591163cbae77f3))
* **tax:** based on actual population ([1de9373](https://gitlab.com/karen/terrence/commit/1de9373d86a7c35e7e4a2adb6a75005b45cd7960))
* **win:** stats in win screen ([7775f20](https://gitlab.com/karen/terrence/commit/7775f20471df57b6bd61c07234a92dd43d12b389))
* **win:** stats when winning ([4ec03ef](https://gitlab.com/karen/terrence/commit/4ec03efd898d57a8470bc67e12d63d9df288ce17))
* add gtag ([3ac6b84](https://gitlab.com/karen/terrence/commit/3ac6b84fbe8a7ed5d53c6123d34f24414ee4db07))
* add homepage ([3ae5858](https://gitlab.com/karen/terrence/commit/3ae585847d2c735c961048908b2abe0f9b16d115))
* bg color intermediary screens to represent seasons ([320284c](https://gitlab.com/karen/terrence/commit/320284cdb2d88615a1906183b2687714591b5e9e))
* **mobile:** android metadata for edition / update ([aa00e49](https://gitlab.com/karen/terrence/commit/aa00e49c2e0cb49da45069e444a0af9d700c7f29))
* **runner:** improve bots ([beb0fb5](https://gitlab.com/karen/terrence/commit/beb0fb5e5fd63f34171058519ad827d5a61b49f3))
* **runner:** new statistics ([c6c8703](https://gitlab.com/karen/terrence/commit/c6c8703d99c38c867f4ecdb19cb0b5c1a1f99c35))
* automate build for android ([a414214](https://gitlab.com/karen/terrence/commit/a414214d99d0c3bb4f64ca9921f80cd05955a994))
* **infra-cards:** integrate images ([6ea86a7](https://gitlab.com/karen/terrence/commit/6ea86a7deeba69126cba39c9dcca6b406a5a4467))
* **ios:** add splashscreen ([32dd066](https://gitlab.com/karen/terrence/commit/32dd066bfb6a353902b58d4992597930bd5d039f))
* **pwa:** update icon ([7890936](https://gitlab.com/karen/terrence/commit/78909361c7b0323fb27115a13d2eeb386ef47570))
* **readme:** add banner and discord link ([64be716](https://gitlab.com/karen/terrence/commit/64be716977f195e71be0b6a0915c367ef53abc8f))
* **services:** foldable cards ([00bff0a](https://gitlab.com/karen/terrence/commit/00bff0a7db30b7491fa0e25b3572f0629852b4ba))
* **stats:** display arrow to indicate expand ([3de9999](https://gitlab.com/karen/terrence/commit/3de99999e099fc27502b25250aa065fa06321450))
* **stats:** stats menu revamped ([fcf7c6b](https://gitlab.com/karen/terrence/commit/fcf7c6b699b046d8b1be49384aa554dd394332c4))
* **title:** animation ([c0efe14](https://gitlab.com/karen/terrence/commit/c0efe14bc589bd204c93b9f3bd39011662d774c7))
* **title:** use new logo ([fd8f4c4](https://gitlab.com/karen/terrence/commit/fd8f4c466dc352b2acac848f79f56903bde0842f))
* **tweets:** integrate new avatars ([4855512](https://gitlab.com/karen/terrence/commit/48555126b51197903c2c2392563d03a467f98836))
* new font ([1c24e3b](https://gitlab.com/karen/terrence/commit/1c24e3b80423bf007b9221fb89856478fe9c19a3))
* popularity indicator ([3e7fba3](https://gitlab.com/karen/terrence/commit/3e7fba30044e2badf727d1bc1e1d5259ce94cb86))
* reach goal modal ([14af9b1](https://gitlab.com/karen/terrence/commit/14af9b1caee4290e5e12573b811ac1ff0a8e8518))
* remove stock selling ([9733780](https://gitlab.com/karen/terrence/commit/973378002553c64a60f3b503920a8ecdd389b39d))
* **bots:** new bots that sells share after turn 30 ([e0ba607](https://gitlab.com/karen/terrence/commit/e0ba60746144a0dcf35def6881f36c43a0029140))
* **button:** new color "danger" ([3b29b6d](https://gitlab.com/karen/terrence/commit/3b29b6dfaed9afb2fb27d6e67a8c848974c552ef))
* **button:** update style ([5af6368](https://gitlab.com/karen/terrence/commit/5af6368c1eff0266d94a8ff2dd69112cb95df273))
* **card:** animate zoom + fix opacity on discard ([3bcf4c5](https://gitlab.com/karen/terrence/commit/3bcf4c509abca09e44201bd99a29a0e2cff641c5))
* **card:** infra quote shows the type of equipment ([25f105f](https://gitlab.com/karen/terrence/commit/25f105f3d6be5b1eea44053853bb3a867b3b6dc4))
* **card:** shadow animation ([9ffdbab](https://gitlab.com/karen/terrence/commit/9ffdbab9ce79e77a76dd7e0bda7f4aa329fa0855))
* **card:** sync action display with last tweet ([9831684](https://gitlab.com/karen/terrence/commit/983168427db3653bc52bd7848ab749c1ce69edb9))
* **cards:** card content consistency ([f0167bb](https://gitlab.com/karen/terrence/commit/f0167bb90659b0bc8221ebd359c5b60abed8a6cf))
* **cards:** display impact infos ([43d916a](https://gitlab.com/karen/terrence/commit/43d916a7ff1e92351aeb6311cf6af5b268f168c6))
* **cli:** summary reads the population ([00946e5](https://gitlab.com/karen/terrence/commit/00946e551022a191dac74bfbff5f3a970a5bad78))
* **config:** new version - removes advisor ([54f708e](https://gitlab.com/karen/terrence/commit/54f708eafa1593f7c82daa8caeb6bbd93a4db2be))
* **cordova:** link to feedback form ([8391a8f](https://gitlab.com/karen/terrence/commit/8391a8f7744081c023d40a944ec78af13ccaf49b))
* **debug:** debug on triple tap ([cfe38d6](https://gitlab.com/karen/terrence/commit/cfe38d66467cef2b314766e39015933e3ced4da6))
* **design:** gradient in bg ([7a9b362](https://gitlab.com/karen/terrence/commit/7a9b362d1968edf1d11c2e3cc1945eb428043e72))
* **footer:** abbreviate cash to prevent overflow ([d535500](https://gitlab.com/karen/terrence/commit/d5355007dafa948b6389c2bd6fe8fb8c0068c16f))
* **footer:** icon for stats ([da220ba](https://gitlab.com/karen/terrence/commit/da220baebabc45cd9ee6d65ca894cea2107bcc2d))
* **footer:** item for menu ([5b0a2ef](https://gitlab.com/karen/terrence/commit/5b0a2ef9a8f66601e1812db0bf42ec296dd2a46a))
* **footer:** multi button with direct access to stats ([a1f5242](https://gitlab.com/karen/terrence/commit/a1f5242498715bd52493b04b21e2815de48ab703))
* **gameover:** no growth game over ([01705be](https://gitlab.com/karen/terrence/commit/01705be58858bc2ad076f6e955e31ea43d037fd1))
* **gameover:** title screen button ([6178da2](https://gitlab.com/karen/terrence/commit/6178da2902efeefe98f4e890bf00f7ad82d3b6f4))
* **gamestate:** turn is computed as part of the impact ([7b1eafa](https://gitlab.com/karen/terrence/commit/7b1eafac9114f739d35c3f49a59e97b8ebc78b82))
* **highscore:** order by population, limit to 10 ([007b6f1](https://gitlab.com/karen/terrence/commit/007b6f136bc602f35962f64e0fd6339c8a4b770d))
* **infra:** get the smallest cost infra choice ([3459002](https://gitlab.com/karen/terrence/commit/3459002cec92046bf1e3a38a0e1b0b210c6f9021))
* **tweet:** rename the VCs ([a75a509](https://gitlab.com/karen/terrence/commit/a75a5092d16b654a67947831c8fff6be83ea72cf))
* **win:** loose on time only if population is less than goal otherwise win ([e255255](https://gitlab.com/karen/terrence/commit/e2552552c216668fd0cb0ca6c3fee8acf22b298b))
* about and credit screens ([21ffa80](https://gitlab.com/karen/terrence/commit/21ffa80a049fc0436126e1acd88fb9c948167649))
* external link component ([cf6f9ec](https://gitlab.com/karen/terrence/commit/cf6f9ecc46acd797c8352cbefef573d47ba42bb7))
* quitting game ([19a2165](https://gitlab.com/karen/terrence/commit/19a2165271c2f6c9ff80dd28a832eed4c4bed0db))
* score ([1902305](https://gitlab.com/karen/terrence/commit/1902305485d60c10389666520a6a470590e35da4))
* title screen ([fd3c236](https://gitlab.com/karen/terrence/commit/fd3c2362170bff364342eb1b0881f6fa73527e98))
* **intro:** implementation negotiation intro ([5d9dc53](https://gitlab.com/karen/terrence/commit/5d9dc537001a3f9281a5f41747505275f28ca8a6))
* **plot:** answer from CEO ([f01947f](https://gitlab.com/karen/terrence/commit/f01947f5e46a8da837c18f53271067785a3bc9fb))
* **plot:** card and tweet ([6ddfa97](https://gitlab.com/karen/terrence/commit/6ddfa9717ed58874108d6b2958340e7127b1ee59))
* **plot:** new turn is only for infra ([cd80013](https://gitlab.com/karen/terrence/commit/cd80013b3047e851bbed815e4a9a9f59a97ecef5))
* **services:** access full service data with id ([d0c0e8d](https://gitlab.com/karen/terrence/commit/d0c0e8d3339142b4109e3b378090b76aeecabf54))
* **services:** icons for services ([5a7c785](https://gitlab.com/karen/terrence/commit/5a7c785076e087b21fea2113849c1f20db803f44))
* **stats:** update information displayed ([feec474](https://gitlab.com/karen/terrence/commit/feec474dbc745cab1b1ccc97497e0babfe4f7cec))
* **uistate:** introduce tick ([9de208d](https://gitlab.com/karen/terrence/commit/9de208def9cbe3f1bd7f1c22fd649a722c0572c1))
* end game using game mechanics ([fb1af24](https://gitlab.com/karen/terrence/commit/fb1af2448de8c57cf38fc3d566b55b1aed542c16))
* game goals are now displayed from the start of the game ([8002573](https://gitlab.com/karen/terrence/commit/800257345a5dae96010d4b5d5910fd2d156aa6c8))
* **advisor:** display the infrastructure to buy ([7980849](https://gitlab.com/karen/terrence/commit/798084943d60ba3f66fbc37e74a10b0a96ea54db))
* **analytics:** summarize runs ([1bc9bc0](https://gitlab.com/karen/terrence/commit/1bc9bc06436c65bab21d36d02194315efa045b70))
* **bot:** budget smart / bootstrap bot ([80c2b5f](https://gitlab.com/karen/terrence/commit/80c2b5fc02e158bc8aadaf608692142ab0c5028c))
* **cards:** add details to card UI metaphor ([f87ec0a](https://gitlab.com/karen/terrence/commit/f87ec0a473e28be71cce7d087e02429744c8a44b))
* **cards:** new infrastructure services ([535ee5c](https://gitlab.com/karen/terrence/commit/535ee5c00e7590c3a6b994f84b078f591429429f))
* **cli:** headless game runs ([7142cff](https://gitlab.com/karen/terrence/commit/7142cff3c464ee70f05f8c1b090d69093abce788))
* **config:** allow overwrite when there is a new config ([e5183d0](https://gitlab.com/karen/terrence/commit/e5183d04a27f68ebb7e66862f306493b6300c938))
* **config:** persist configuration to localstorage ([1879cdd](https://gitlab.com/karen/terrence/commit/1879cdd0852597d0a932a0da6d170ed58bdaa372))
* **debug:** can modify UI configuration live ([2a74207](https://gitlab.com/karen/terrence/commit/2a7420782c899a0b057d2ce3772705b64e4581b8))
* **debug:** new debug screens ([2418b5a](https://gitlab.com/karen/terrence/commit/2418b5a35e5cc4cae3f3905eea71fc1105a90e4e))
* **debug:** scrollable modal ([85596ea](https://gitlab.com/karen/terrence/commit/85596ea3b972df498aa0f4074dd68bca7544d030))
* **footer:** add colors and fix alignments ([b01025c](https://gitlab.com/karen/terrence/commit/b01025ce37a5e3ec4d2e0e3e9566adfff35d92c3))
* **footer:** display bank balance ([54148ec](https://gitlab.com/karen/terrence/commit/54148ec98dded5d4dee96f7f6bf666beb40f9d30))
* **happiness:** decision streaks ([840fb87](https://gitlab.com/karen/terrence/commit/840fb87abf7511eb37196c76a3c455e3d8975a02))
* **infra:** readable quote ([f174ba0](https://gitlab.com/karen/terrence/commit/f174ba0cee15b1ad98e3a91ad16e0b7d5ca0c5f0))
* **layout:** background image ([64ccd7e](https://gitlab.com/karen/terrence/commit/64ccd7e7758e51f99e5044afca9e7f670c545099))
* **modal:** full height modal ([c2893c1](https://gitlab.com/karen/terrence/commit/c2893c1fa44356e36bb4f8eb5b6fcb398da32d8a))
* **modal:** implement transition ([6df9a4a](https://gitlab.com/karen/terrence/commit/6df9a4a5809e2c31e00d972c3a2dda75c85bb694))
* **newturn:** animation timing for readability ([f0b07a5](https://gitlab.com/karen/terrence/commit/f0b07a562c2652771f0a669944d00bb9f02d0e29))
* **newTurn:** display a screen for each turn ([763362b](https://gitlab.com/karen/terrence/commit/763362bba1ad3f691924de9380664828c7a9d6a8))
* **stack:** use tailwind css ([98a4fdc](https://gitlab.com/karen/terrence/commit/98a4fdc43ae5dcbdd5a3cd8ec3b69a557a4830cc))
* **timeline:** add colors ([d9a1613](https://gitlab.com/karen/terrence/commit/d9a161367e7249b5973591561b58067b84fcd00f))
* **tutorial:** bounce if no action from user ([5356aaf](https://gitlab.com/karen/terrence/commit/5356aafc84bd9e06c06e6a46cd21ba9499342538))
* **tutorial:** break the 4th wall to explain the swipe ([67bf46a](https://gitlab.com/karen/terrence/commit/67bf46afc380fa43792c278dd2aec813febe7ab8))
* **tweet:** happiness in tweets ([12c8f01](https://gitlab.com/karen/terrence/commit/12c8f012ff3cee75c7c06e1b367c5530c16deacd))
* **tweets:** answers are underneath the original ([df12d60](https://gitlab.com/karen/terrence/commit/df12d604da82c45441883cc6bb61e20437e49f0e))
* add an intro story screen ([b82c37f](https://gitlab.com/karen/terrence/commit/b82c37f147e63ea77d8c06cf838a819fc12efdb8))
* cards as documents ([54e71cd](https://gitlab.com/karen/terrence/commit/54e71cd7db666d750f8bdaf1042927d6236069e5))
* CEO answers the citizen tweets ([936997f](https://gitlab.com/karen/terrence/commit/936997f98fa7fd89006102945284680debfde674))
* custom variable stocks offer ([4a6ea20](https://gitlab.com/karen/terrence/commit/4a6ea209bd9f730828795205a5dbee87acf4fa1a))
* growth steps rules ([ad12cf7](https://gitlab.com/karen/terrence/commit/ad12cf7ad6bc6476d2e0fda623105bcb6c625094))
* implement timeline of tweets ([fffd0ef](https://gitlab.com/karen/terrence/commit/fffd0ef24211d01f57c0d5393ea38dfcc130bfb8))
* tweets about the growth of the city ([fb8463a](https://gitlab.com/karen/terrence/commit/fb8463a7de9908954960f3789b34274918df2372))
* **transition:** allow onExited callback ([a5bd501](https://gitlab.com/karen/terrence/commit/a5bd50125b0ed0a2357577afb203bc73a19898ab))
* **transition:** onEntered callback ([5949542](https://gitlab.com/karen/terrence/commit/594954257131fe501662e0468568647089535fb9))
* **tweet:** avatars for all ([1521de4](https://gitlab.com/karen/terrence/commit/1521de4ea3ace40fa9810234b60ca4e016e73e4e))
* **tweet:** random aparition of stock card ([b85e364](https://gitlab.com/karen/terrence/commit/b85e364511268387e49ec785a7305d1e43515ea5))
* **tweet:** random hashtags with infra ([95f0447](https://gitlab.com/karen/terrence/commit/95f0447597f1dfb8433571bfb61c6e278ee11643))
* **tweetgen:** tweets should have ids ([b1be091](https://gitlab.com/karen/terrence/commit/b1be0910c027d614bf6af0048fda7adae394e3a0))
* **tweets:** icons for realism ([b07d95d](https://gitlab.com/karen/terrence/commit/b07d95dcc6af0ef62d5508bd9cbe18ff447e8788))
* implement game end ([d64e038](https://gitlab.com/karen/terrence/commit/d64e03826977f33da5a4120f4e3687847050d223))
* implement visual label for swipe action ([70c10cc](https://gitlab.com/karen/terrence/commit/70c10cc32070dd852369758f1a382808f8d4d052))
* new card type: stock selling ([6f54a55](https://gitlab.com/karen/terrence/commit/6f54a553986f12c4f3cf433326e36619b3d34a8e))
* **UI:** allow transition before next tweet ([3413405](https://gitlab.com/karen/terrence/commit/34134058e74791f6d9fe5b1e1b168c517c3bec6d))
* **uistate:** information about new game / end game ([2d6a38c](https://gitlab.com/karen/terrence/commit/2d6a38ca3c29656fa8a4984779629d09b0b04730))
* implement an advisor that explains the costs of the action ([6f2535d](https://gitlab.com/karen/terrence/commit/6f2535d2cfb97abb66ee29393931eb584066ff33))
* implement debug button with turn display ([3759d8a](https://gitlab.com/karen/terrence/commit/3759d8afa296620bf0a30c92c59a1b8f2baccd8e))
* integrate feedback form ([7227269](https://gitlab.com/karen/terrence/commit/72272697e61bcee209381dc9de0d303d0517bb96))
* preview tweet as we move the cards ([fc05629](https://gitlab.com/karen/terrence/commit/fc0562906c122d1f21371925c4315eee9b1ee965))
* PWA ([ab86578](https://gitlab.com/karen/terrence/commit/ab86578a1ffd562773b8c08f902938bc4d905269))
* screen for statistics ([faf8802](https://gitlab.com/karen/terrence/commit/faf8802790e6be145c8351a56c54e7a941e4e1f8))
* track game events with Amplitude ([4bf170f](https://gitlab.com/karen/terrence/commit/4bf170f7f99766d7491ab2c4eaf299790fc5046a))
* **ui:** new layout ([a37978c](https://gitlab.com/karen/terrence/commit/a37978c3243f9b0530b9702f86d7032652942947))


### Bug Fixes

* **amplitude:** add tick in GAME_DONE and ACTION_PICKED ([3d98136](https://gitlab.com/karen/terrence/commit/3d98136d763fda6ceb3e790c294b2eb530e3114d))
* **android:** update store assets ([4ca5b61](https://gitlab.com/karen/terrence/commit/4ca5b61ca0134cdd261515a57e659f92dd7cad77))
* **async:** make amplitude calls async... ([437217d](https://gitlab.com/karen/terrence/commit/437217d9c372b1e7ce159048a5a8988f434f15f8))
* **balance:** more rent ([e60aed6](https://gitlab.com/karen/terrence/commit/e60aed6281000e64a28a4128e8a5716e252b6eeb))
* **card:** display card after the new turn ([cdce5bd](https://gitlab.com/karen/terrence/commit/cdce5bdb90ec980ac246cdd706211552a2d6c3d4)), closes [#20](https://gitlab.com/karen/terrence/issues/20)
* **card:** top of the screen should be swipeable ([cadd54b](https://gitlab.com/karen/terrence/commit/cadd54bfb0ef013c383caf10814490accf82ef4a)), closes [#24](https://gitlab.com/karen/terrence/issues/24)
* **CD:** add CI options for chromatic ([9ab0e9d](https://gitlab.com/karen/terrence/commit/9ab0e9d200c1d25d40a3ff66767b14d37d03d0f5))
* **CD:** cp index.html to 404.html for SPA flavor ([3fcd078](https://gitlab.com/karen/terrence/commit/3fcd07851a78d9d2b284569f791382ec6c2fd137))
* **CD:** fix few elements for android deploy ([fbe9fb8](https://gitlab.com/karen/terrence/commit/fbe9fb844b9eed5f4cfa649a0618cb2400df2972))
* **CD:** misc. fixes ([890baa3](https://gitlab.com/karen/terrence/commit/890baa30ac16baa9e1e4df401cddc042847b0354))
* **CD:** run chromatic on a different job ([6984593](https://gitlab.com/karen/terrence/commit/6984593e3800b6eb4b23faa5b0618aeb8a725575))
* **CD:** use chromatic for story deploy ([38b4f8e](https://gitlab.com/karen/terrence/commit/38b4f8ec97eb14f9ad29397899a7048b8048d754))
* **config:** make config hook compatible with storybook usage ([fa7a72d](https://gitlab.com/karen/terrence/commit/fa7a72d55098b05fa23138c23b65f59110326f19))
* **contract:** refusing the first update should work ([9287d99](https://gitlab.com/karen/terrence/commit/9287d994b38d95008ffd2d7e0c633921cc5c6008))
* **debug:** data is selectable ([4e24ac0](https://gitlab.com/karen/terrence/commit/4e24ac0069432870365a72572582c776ebe4abd9))
* **debug:** update event handler ([a07c1f4](https://gitlab.com/karen/terrence/commit/a07c1f4608735120463b50181136b30240b25083))
* **discordlogo:** allow font color ([cbb17f4](https://gitlab.com/karen/terrence/commit/cbb17f4f17e7a7ccbf7c9276187d4be5c11de5b0))
* **events:** predicate must not always return 0 ([02aad4e](https://gitlab.com/karen/terrence/commit/02aad4ec2db3e946431b9b13a11ea6766796dac1))
* **expandcity:** more chance of expanding the city ([dfaec9c](https://gitlab.com/karen/terrence/commit/dfaec9ce955a2afbcad10a6affc1429b245edbab))
* **gameover:** add illustration for no growth and chaos gameovers ([3dd8280](https://gitlab.com/karen/terrence/commit/3dd8280760f7de072bb676532052c22b47f84530))
* **gameover:** CTA leave a feedback ([d0a95ad](https://gitlab.com/karen/terrence/commit/d0a95ad9889310b95a8bddbc3bcc2077ba285cf4))
* **gameover:** no growth on first levels ([3085b60](https://gitlab.com/karen/terrence/commit/3085b60c4028f8c3de5ceb2fde0749422525d5d9))
* **gamestate:** allow only the number of available plots ([d19253c](https://gitlab.com/karen/terrence/commit/d19253c6512f100ca99dd7ab570b9043562311bf))
* **generic-card:** remove old transition implementation ([271ef08](https://gitlab.com/karen/terrence/commit/271ef0828c0d784cb9c4319406d308ed809c41b5))
* **goal:** unreachable button on small screen ([13321ca](https://gitlab.com/karen/terrence/commit/13321ca6ec93b380375bc7ffced9e631f9aefe24)), closes [#14](https://gitlab.com/karen/terrence/issues/14)
* **home:** center main banner text ([905ae56](https://gitlab.com/karen/terrence/commit/905ae5696c8cb8356183bd47cec4665b070c4044))
* **home:** links underline, not cards ([644e728](https://gitlab.com/karen/terrence/commit/644e728a7f84e07bef8cc9efb5feb2e351277172))
* **homepage:** new badges for apps ([5616127](https://gitlab.com/karen/terrence/commit/56161273699cf8af84e6a70a5a06cc497e42a593))
* **icons:** sizing ([c9c3a00](https://gitlab.com/karen/terrence/commit/c9c3a006c44649e035d9f988b0e191e1bce992d0))
* **infra:** design fix ([cd1f67d](https://gitlab.com/karen/terrence/commit/cd1f67d8f0a092ca41d26c99392f4d67ccb7f3b9))
* **infra:** prices update ([93c84ac](https://gitlab.com/karen/terrence/commit/93c84ac02a45475d388defc8ab246a6b5a9234e8))
* **levels:** screen update ([1063709](https://gitlab.com/karen/terrence/commit/10637095e9050bd0c9959fd9105db7f639a88744))
* **newturn:** display population only if > 0 ([2bd48b3](https://gitlab.com/karen/terrence/commit/2bd48b3360f46c9bba485aedd4bdc353e148e14f))
* **newTurn:** better population animation ([724c139](https://gitlab.com/karen/terrence/commit/724c139b59a687373e1f0fb84279c7f4900a34b1))
* **newTurn:** only display level when > 1 ([157940b](https://gitlab.com/karen/terrence/commit/157940b8a302ce9243c1450bc8ec2ce9e3387500))
* **pickCityName:** prevent glitchy names after selection ([6afd05a](https://gitlab.com/karen/terrence/commit/6afd05afab0a6d87f87c2a381edb928284366529))
* **pickname:** prevent key clash ([4a67210](https://gitlab.com/karen/terrence/commit/4a672106ee4af4ed9d128a4fbac3c6820c660f07))
* **plot:** expansion answer tweet should be consistent with action ([8d816d9](https://gitlab.com/karen/terrence/commit/8d816d9df309db2bd9fc3fe932d8d1037a58cdbd))
* **prices:** reduce price of gravel road ([7c3914d](https://gitlab.com/karen/terrence/commit/7c3914dcf137cdf1d2656fb59eb5c431b3c9ee4a))
* **screenshots:** fix scenario ([8f63ebf](https://gitlab.com/karen/terrence/commit/8f63ebf0f1704e105df8afbe72c3811b532f9656))
* **service:** correct level matching ([29d36ce](https://gitlab.com/karen/terrence/commit/29d36ce6fce3dc32c25852dab3771c1b5c2b286c))
* **serviceworker:** popup hidden ([db2a797](https://gitlab.com/karen/terrence/commit/db2a79730d536b708a4851cc60a813ca344acbf8))
* **stories:** better organization for event cards ([6ec178c](https://gitlab.com/karen/terrence/commit/6ec178cd705b9c7f10d5d597c34ff0e0a0d5c479))
* **tutorial:** display the tutorial with the first card ([e6d0040](https://gitlab.com/karen/terrence/commit/e6d0040d35024b59bbbf231dfb26e836541e6583))
* **twee:** correct service fetch ([8c84c75](https://gitlab.com/karen/terrence/commit/8c84c75a8d1f657187bf6b7fdb05d797af10dac8))
* **ui-state:** flag to identify actionable tweets ([db55447](https://gitlab.com/karen/terrence/commit/db55447f881377ca77780f6fed8fd4c06f206531))
* **update:** bring popup to front ([ed3f3b4](https://gitlab.com/karen/terrence/commit/ed3f3b4d230d168d5095f4e91840963f0bfcf5f0))
* android build ([23b0d71](https://gitlab.com/karen/terrence/commit/23b0d718fb8a3d59d816a67d6c10783922cb8826))
* city name should appear in tweets and comments ([c7e50e8](https://gitlab.com/karen/terrence/commit/c7e50e8f2024fd206bc53496cbabcd5e1e4e5984)), closes [#8](https://gitlab.com/karen/terrence/issues/8)
* copywriting contract card ([7975245](https://gitlab.com/karen/terrence/commit/7975245580945d6f995b2e9d377c41f5f5ead39c))
* update social message ([eaf769d](https://gitlab.com/karen/terrence/commit/eaf769da3662b5c1088eb222414c26ca3a437ee7))
* **app:** special behavior in app context ([b155497](https://gitlab.com/karen/terrence/commit/b155497d526f5c7411ab5117e662382200ac23c3))
* **art:** update game over modal asset ([beebfee](https://gitlab.com/karen/terrence/commit/beebfee0fe8f7813b22308fbfcc764e8fa0b3b71))
* **assets:** update ([1ef0a13](https://gitlab.com/karen/terrence/commit/1ef0a135194b226a9decfb9b61518cf785cdd9c6))
* **base-button:** external links open in new window ([ee41d16](https://gitlab.com/karen/terrence/commit/ee41d16429b3ccd2c0dfc3617f2df63f4816164c))
* **card:** add top margin ([4e653a4](https://gitlab.com/karen/terrence/commit/4e653a41fc6763d872f4b19a1f646fdf39b46dd8))
* **card:** animation config if is idle ([877f9f3](https://gitlab.com/karen/terrence/commit/877f9f38d106a28e470aa86cc6a9a4745093b9a8))
* **card:** idle timer back to reasonable time ([bb17915](https://gitlab.com/karen/terrence/commit/bb1791513cfaf118103a7148dc145b476b173003))
* **card:** restore bounce and add a debug config to disable ([996b1b6](https://gitlab.com/karen/terrence/commit/996b1b6a9733747baeb4f63e305dcfcac165aca1))
* **card-content:** display of images based on space avail ([e8b34ed](https://gitlab.com/karen/terrence/commit/e8b34edf282c32153236e3555c1b83f1d7286643))
* **card-content:** fix stack of elements ([ed2f000](https://gitlab.com/karen/terrence/commit/ed2f00091138c428c89ca9d9e5fb72a905b0e131))
* **cards:** infra cards new format ([2aa20e9](https://gitlab.com/karen/terrence/commit/2aa20e9a870162bac8a12319669ccb832b1982ef))
* **cards:** svg optimization ([4225aec](https://gitlab.com/karen/terrence/commit/4225aecba15ef7a3843a85a0457f8f34db12e5f3))
* **cards:** when there is no one, there is no one to ask ([b43a50f](https://gitlab.com/karen/terrence/commit/b43a50f79f1b941322e26556472feb4770c8028f))
* **contexts:** different home based on app/web context ([ee8c53a](https://gitlab.com/karen/terrence/commit/ee8c53a4777283d31a58d9d68a7b2142d0f98a54))
* **stats:** rename tax to rent ([455923b](https://gitlab.com/karen/terrence/commit/455923b498129cb5d7e38095e99c7006aacbcd04))
* citizen growth on market price ([650b55a](https://gitlab.com/karen/terrence/commit/650b55a64ec38a62214a94baf030fc42b62d33de))
* **categories:** label for internet ([d5f5865](https://gitlab.com/karen/terrence/commit/d5f58658bb3c637f1e998f95b94f5b36e191efed))
* **css:** optimize css for production ([5868cda](https://gitlab.com/karen/terrence/commit/5868cdaa3afa7b0d5567b19fd57ddfe26f1f22f2))
* **footer:** optimize icons for display on iOS ([82baef5](https://gitlab.com/karen/terrence/commit/82baef5f812b63b19f8eb3e1bab746a1de941de8))
* **footer:** safari bug with buttons and positions etc ([c05011c](https://gitlab.com/karen/terrence/commit/c05011ca1351c56b4d35952cb70d0f9f0efa3f83))
* **format:** display infinite when number is not finite ([b0bdeaa](https://gitlab.com/karen/terrence/commit/b0bdeaa7c307469a57283105d12e0b932c1df0dd))
* **format:** units and precision 0 ([99fc7f1](https://gitlab.com/karen/terrence/commit/99fc7f1d542faa80d03275ffbd5580d33b6cce72))
* **gamedesign:** balance the cost of services ([1f5cf80](https://gitlab.com/karen/terrence/commit/1f5cf8041cf9745d2d5708216852ccac2fd5f398))
* **gamedesign:** step the growth curve based on services ([4283c08](https://gitlab.com/karen/terrence/commit/4283c08476a4f35241bd070d2e22e73767124003))
* **gameover:** only display one explanation ([0b83445](https://gitlab.com/karen/terrence/commit/0b83445a4272abb92f3712b1d7f11aaf9979d7dc))
* **gameplay:** trigger next turn after contract update + 0 pop ([879256d](https://gitlab.com/karen/terrence/commit/879256d992ff60607c54dfb45396af895e0bcb3f))
* remove unnecessary google play button ([526b8d4](https://gitlab.com/karen/terrence/commit/526b8d4c9dc523aaf995d8b9e26dfea6ecef1c6f))
* **headless:** import packages from src ([ef6dc4e](https://gitlab.com/karen/terrence/commit/ef6dc4e898a6c2fffe5c7e002b839395b09941d9))
* **highscore:** save highscore when winning ([92c42b1](https://gitlab.com/karen/terrence/commit/92c42b13befed678b228ae354cf582f8bda3716d))
* **icons:** web icons update ([436ff83](https://gitlab.com/karen/terrence/commit/436ff83a9bab7d568aecdc0fdf6e884871690805))
* **ios:** update status bar configuration ([90896c6](https://gitlab.com/karen/terrence/commit/90896c6c383599874e5b2a2e5b40b60fa94ac6d1))
* **mobile:** breakpoint for screen width ([7e7e8e2](https://gitlab.com/karen/terrence/commit/7e7e8e2e263c29fd5b1988e777a76bff93ed3ae1))
* **modal:** content expanding ([9246c9c](https://gitlab.com/karen/terrence/commit/9246c9c1be42949e92efde5eac0659d842fa4dde))
* **modals:** update style ([3d95b20](https://gitlab.com/karen/terrence/commit/3d95b20523998ce0a3ade741eac07ed14474fdee))
* **popularity:** iOS height ([684b9f9](https://gitlab.com/karen/terrence/commit/684b9f977be01b904f7e33766ce94830f4797219))
* **population:** update progression ([4f5e173](https://gitlab.com/karen/terrence/commit/4f5e17360aa9f43109b2adef0fe3dde5cd105419))
* **runner:** misc improvement ([1accfba](https://gitlab.com/karen/terrence/commit/1accfba3fda27f51f99187077a2592038b1e091f))
* **service:** If no service definition exists, create one ([e916ee5](https://gitlab.com/karen/terrence/commit/e916ee5c1eefd5b1bdb6cb1b2f1aba3b0c2da5de))
* **services:** undefined services lvl ([5ba4594](https://gitlab.com/karen/terrence/commit/5ba4594a3c3fdebe4ca088cb47daeb2d452257cb))
* **stats:** fix scroll ([d1c5663](https://gitlab.com/karen/terrence/commit/d1c5663c576172eba160cb82aab63e18ec9e1091))
* **stats:** trying to fix iOS scroll ([96aae13](https://gitlab.com/karen/terrence/commit/96aae13fe93ef5ce42a6a70bf3e36fb1f7ccdbac))
* **stocks-card:** colors ([8979057](https://gitlab.com/karen/terrence/commit/8979057d7bf3225aad186add5b9702609b84e4f2))
* **stocks-card:** infinity is not valid ([cda3b51](https://gitlab.com/karen/terrence/commit/cda3b51808219a5cc68525fa5fe84a7e19660d93))
* **stocks-card:** percent of total of shares ([23d42da](https://gitlab.com/karen/terrence/commit/23d42da7fd43d1e71cb40a1e952cd951932fb2f2))
* **svgexport:** output filename ([207d55a](https://gitlab.com/karen/terrence/commit/207d55ae1d5fe0ac5d460a2e79bd1c6001c7629f))
* **timeline:** only display and animate 10 last tweets ([97b4a62](https://gitlab.com/karen/terrence/commit/97b4a6251ec6f64fe7a2d7393d027ef0b973eb07))
* **title:** image should be full width on phones ([a787d9e](https://gitlab.com/karen/terrence/commit/a787d9e32a86ecfd816e050f34ed71401fdee711))
* **turn:** should work with -1 to -3 turns ([34f1eaa](https://gitlab.com/karen/terrence/commit/34f1eaa47188e7a952e2a32440f16f3b9284f37e))
* **ui:** goal reach only once ([0bceee7](https://gitlab.com/karen/terrence/commit/0bceee754128d73811a68385a8fbdc01203f0dd1))
* update terrence to citerate ([7115b47](https://gitlab.com/karen/terrence/commit/7115b47300cd0d517f02f9adfc7c52f0c9b79249))
* **buttonLink:** full width ([2d375aa](https://gitlab.com/karen/terrence/commit/2d375aa6b5967083612dcda617b32f00262709e7))
* **card:** isIdle should be reset when swipe ([770fa1d](https://gitlab.com/karen/terrence/commit/770fa1df23aa66e422f34b8c17cdb671b8e19561))
* **card:** position at bottom ([43b3b96](https://gitlab.com/karen/terrence/commit/43b3b96e9144b9590f2aaeb49fc12df272c4b52f))
* **card:** use springs back ([55da458](https://gitlab.com/karen/terrence/commit/55da4581c2cea4d836364621007aafff826b824d))
* **cd:** correct domain for production ([14da798](https://gitlab.com/karen/terrence/commit/14da798b2eafbb707c89963fbb63483ef58793b2))
* **content:** label of link to title screen ([bad78cc](https://gitlab.com/karen/terrence/commit/bad78cc6c033ccb2090f73fc726beed5cf04837a))
* **cordova:** cordova detection was broken on mobile and dev ([2a99ca8](https://gitlab.com/karen/terrence/commit/2a99ca8de45f17caa93a7b5d0af444d5d59c6d5c))
* **cordova:** id and name update  ([4124bf1](https://gitlab.com/karen/terrence/commit/4124bf1ca461748dfa1a2e5c00d99d4bce674bf9))
* **debug:** scrolling ([d104842](https://gitlab.com/karen/terrence/commit/d1048425d2540be59fc987c7c4b4fdf3b0e3d4c0))
* **design:** tweak sizes ([5b88373](https://gitlab.com/karen/terrence/commit/5b88373e753be05bf9097fbf1b81d9a980284076))
* **favicon:** replace react one ([964fde5](https://gitlab.com/karen/terrence/commit/964fde5c55e3e46ea53db9c15b3d86e9935675b3))
* **footer:** full button surface is clickable ([120f78b](https://gitlab.com/karen/terrence/commit/120f78b79c3d8df31930ddb67e53771aa4e8d1ab))
* **footer:** option item label ([9f789d7](https://gitlab.com/karen/terrence/commit/9f789d7f7d76a23a4bc993040edbdfc5b4514049))
* **footer:** remove margin bottom ([d3ceb05](https://gitlab.com/karen/terrence/commit/d3ceb054515bc4c132b81059401356d8d9b1b0f3))
* **format:** add trillions ([97bee3d](https://gitlab.com/karen/terrence/commit/97bee3d30d5635374513e7618ae0bc4b184b7c6a))
* **gameover:** center text ([a157531](https://gitlab.com/karen/terrence/commit/a157531d7bcd6387c1aa3e6e4f010386f2b297c6))
* **gameover:** proper message when no terrain has been bought ([499d844](https://gitlab.com/karen/terrence/commit/499d8440a39491b9a7f0639067563eb4606fb048))
* **icons:** smaller icons in footer ([15db6bc](https://gitlab.com/karen/terrence/commit/15db6bcce750931a46f0c0c276478bf39448afd8))
* **index:** update name ([613985b](https://gitlab.com/karen/terrence/commit/613985ba7113bc68c6a9066dea3e576dca03c3f6))
* **infra:** cost is abbreviated ([bb684bf](https://gitlab.com/karen/terrence/commit/bb684bf7b5602d4fefb3742c5e4a4ac514df2bc2))
* **initialstate:** consistency b/w population and plot sold ([f651db9](https://gitlab.com/karen/terrence/commit/f651db9bc1bf588ccc3eb83b173edbf7f084eaae))
* **intro:** do not display stocks if not meaningful ([d885519](https://gitlab.com/karen/terrence/commit/d885519afb51a42a6489122078ad0c89e783cf76))
* **intro:** remove swipe in tweets ([9185b8f](https://gitlab.com/karen/terrence/commit/9185b8f3776a0a9961758ea2c82a32cc4d6f416c))
* **intro:** update hints and content ([bbd502e](https://gitlab.com/karen/terrence/commit/bbd502e7affc4fcd21231145cbe78cf617c2d9e1))
* **ios:** body scroll lock ([38fb304](https://gitlab.com/karen/terrence/commit/38fb3043598e22dd67e232454d841b1fed6b5004))
* **layout:** max height ([6073d85](https://gitlab.com/karen/terrence/commit/6073d859165c508b15215d8557fd487702957ee7))
* **manifest:** update game name ([4d4b4e4](https://gitlab.com/karen/terrence/commit/4d4b4e41cf7309183cd9f07c9b98f0c75ee9233d))
* **overlay:** solid background ([e8093b1](https://gitlab.com/karen/terrence/commit/e8093b1f1b621fc47242449d2f7c5b1a6e747ecf))
* **perf:** jiterry card swipe ([44ac8bf](https://gitlab.com/karen/terrence/commit/44ac8bf996abb4017b87b272ff697fe97caef966))
* **plot:** build at least 10 plots ([25f7304](https://gitlab.com/karen/terrence/commit/25f730453c1883c5dfebe8f5fbee6cae24f6b5f1))
* **pwa:** standalone > fullscreen ([e6f744a](https://gitlab.com/karen/terrence/commit/e6f744a59ba8048383da1128740f2fd100476c46))
* **readme:** add contribution guide link ([12762a7](https://gitlab.com/karen/terrence/commit/12762a79ed94b230cfaf5f6a4e1c50eda390b4b6))
* **responsive:** size for small screens ([741cc00](https://gitlab.com/karen/terrence/commit/741cc00b7f08ca51fa45f5f984885fd63cd1c680))
* **responsive:** tweak sizes to fit 3 tweets on the smallest screens ([df6f634](https://gitlab.com/karen/terrence/commit/df6f634b85b417e4355dd1854404e4dc5ea263ab))
* **scroll:** remove unnecesary scrollbars ([6570f6d](https://gitlab.com/karen/terrence/commit/6570f6d52fa2d94feec02c3a820358c397a6c41d))
* **stack:** adjust elements in z stack ([0d98454](https://gitlab.com/karen/terrence/commit/0d9845441473c6658a07ed7e4ec6abb7ed5b9127))
* **title-screen:** spacing in menu (and add title name) ([22423e0](https://gitlab.com/karen/terrence/commit/22423e08110bc04d1a57a1ac7b1a1cb4183ff48d))
* fix link game intro to title ([e16d47e](https://gitlab.com/karen/terrence/commit/e16d47e7845ceaac67f33fd68f213e360ce44b45))
* no selection ([1ae26a1](https://gitlab.com/karen/terrence/commit/1ae26a1f0e581cba769edebdaf9d11a2b14ade30))
* **timeline:** fix size to 40% of screen ([970c7a1](https://gitlab.com/karen/terrence/commit/970c7a195c0e71ee6382300b8ee7ce7f786dde65))
* provide history to stats screen ([5cb7435](https://gitlab.com/karen/terrence/commit/5cb743588440fea34a8706eeab427d2aa3fd310e))
* remove newspaper tweets ([9e565b9](https://gitlab.com/karen/terrence/commit/9e565b9cb7cbe8560222ade3745c5952aa33abf0))
* title typography ([28ffc2e](https://gitlab.com/karen/terrence/commit/28ffc2e13834826fca5005ce0bca704d307482ec))
* update some key config + auto update from xcode ([4fb2029](https://gitlab.com/karen/terrence/commit/4fb2029094fe8d63988155bf500d70e5b6624cd3))
* **advisor:** do not display in game over screen ([112c3b4](https://gitlab.com/karen/terrence/commit/112c3b449cb39bfe871f1df382780e1737c50e79))
* **advisor:** make it more obvious ([6887b08](https://gitlab.com/karen/terrence/commit/6887b0820b24f93e187f234c1abe2fb2cd74aed7))
* **advisor:** some text for stock selling ([8be1727](https://gitlab.com/karen/terrence/commit/8be1727aa6888ec5cbb8078b44e6b4aea876ae68))
* **card:** make decision threshold closer to the center ([d9c8f79](https://gitlab.com/karen/terrence/commit/d9c8f79d4abc9fbc3e5a9529111be6c62d5ea0e5))
* **card:** preview tweet for plot ([a1a8b10](https://gitlab.com/karen/terrence/commit/a1a8b106bbe1d59246663dd0f03170591f81b0de))
* **card:** smoother card swipe ([b1bc845](https://gitlab.com/karen/terrence/commit/b1bc845a5f254f4da408a22189b7188c09139356))
* **card:** tweak animation ([796fa53](https://gitlab.com/karen/terrence/commit/796fa536e44943f58ca416d12670f84348c9ee6c))
* **cards:** dynamic card size ([35925d1](https://gitlab.com/karen/terrence/commit/35925d1bdda73813eb5bdd17d5dab6f07c982d73))
* **ci:** add `homepage` key for `npm run build` root public setting ([63f28c2](https://gitlab.com/karen/terrence/commit/63f28c2f91181bd1b931e29ebd00685befc839db))
* **ci:** remove already existing public folder ([7fc487d](https://gitlab.com/karen/terrence/commit/7fc487dfb74a4254cf808a7a3e739704b2794981))
* **debug:** add label to closing button ([52c6634](https://gitlab.com/karen/terrence/commit/52c66349c0f141d8ba54c5d71c7a309dee1c2759))
* **footer:** prevent the footer overflow ([d5dcbc3](https://gitlab.com/karen/terrence/commit/d5dcbc3c233c96a870d28f2802aedd2678abcfa8))
* **format:** currency formatting ([b9e362f](https://gitlab.com/karen/terrence/commit/b9e362feb943bc620c3e41a5dc21cb9bd27e4e55))
* **gameover:** alt text for images ([6748af4](https://gitlab.com/karen/terrence/commit/6748af436f2541edb0d0dc24b845ef785242ab48))
* **gameover:** custom message for lose condition ([00d8d06](https://gitlab.com/karen/terrence/commit/00d8d06078ffad28a42e9594bcd2ea4105e062f1))
* **gamestate:** do not move turn if win / lose ([fd7d08e](https://gitlab.com/karen/terrence/commit/fd7d08e661c3d55e1ce1535d0abc4be0650e2ba3))
* **gamestate:** do not require tweet for left/right ([c8804c0](https://gitlab.com/karen/terrence/commit/c8804c07ac851a68b8724efb0afacfe017bfd6a0))
* **gamestate:** missing break ([a24cd96](https://gitlab.com/karen/terrence/commit/a24cd9600597e9eb02de4dfa47d6bdcc56d37e36))
* **happiness:** happiness impacts the population growth ([9613b0c](https://gitlab.com/karen/terrence/commit/9613b0c552af5b5105db64db285a19bc2fff02e9))
* **header:** too much information atm ([4831374](https://gitlab.com/karen/terrence/commit/4831374a6e6f2eeb5b00ae27a2658a3c22929845))
* **main:** fixed cards position ([2c00abd](https://gitlab.com/karen/terrence/commit/2c00abd3fd28eb51321be87ee3d638384e402c5e))
* **modal:** fixed height ([19fa422](https://gitlab.com/karen/terrence/commit/19fa4227894cf7d7393ecd4fe16f96edfe4a25e7))
* **newTurn:** display on first turn ([214c269](https://gitlab.com/karen/terrence/commit/214c269af41a91fc4e03e8f1b8e8473e66197628))
* **newTurn:** fix bug on desktop computer ([94e4d1e](https://gitlab.com/karen/terrence/commit/94e4d1e85ed23fbf5145215774e04d2ddcc44d18))
* **plot:** writing tweak ([5c065e3](https://gitlab.com/karen/terrence/commit/5c065e3e85c756d074e077c221804d92499ed172))
* **quote:** text alignment ([4fc4ead](https://gitlab.com/karen/terrence/commit/4fc4ead5918414754fd754c2d3c8a5b1b865fd4c))
* **state:** happiness max 100 ([2358093](https://gitlab.com/karen/terrence/commit/2358093e0f82cd5f525f9b46cd32cc0d60c514bb))
* **stats:** better money label ([96d41d6](https://gitlab.com/karen/terrence/commit/96d41d6876944c5d5bc5d364fc8f869c466de59a))
* **stock:** display price correctly ([fb4e049](https://gitlab.com/karen/terrence/commit/fb4e049c030d1ded9de50827549afc035ebd90a4))
* **tailwind:** 0ms delay ([5d1dd57](https://gitlab.com/karen/terrence/commit/5d1dd5780a5d3c4b442d682d5f7501a422270c2b))
* **timeline:** animation tweaks ([a6f8fc3](https://gitlab.com/karen/terrence/commit/a6f8fc357548299a885a05361c02f714cf81dab3))
* **timeline:** rm mention of the social networ and no border ([2718668](https://gitlab.com/karen/terrence/commit/2718668e1e63afae846c139d301fa7c687e698c6))
* **timeline:** styling details ([b8b4fbc](https://gitlab.com/karen/terrence/commit/b8b4fbc4f9386507c3fab953b0a88d520473ce00))
* **tweet:** move tweet generation to gamestate ([0a044b1](https://gitlab.com/karen/terrence/commit/0a044b1f2f829c726db7175b4d2472d56f9e5461))
* **tweet:** plot answer tweaks ([617a896](https://gitlab.com/karen/terrence/commit/617a8969ffd1ad08c672a2055a27108655927cbf))
* **tweet:** portraits with Alt ([4d402a2](https://gitlab.com/karen/terrence/commit/4d402a2109e04c9435cbcdac54bcb7db4781c1ee))
* **tweet:** random bg avatar ([80edec8](https://gitlab.com/karen/terrence/commit/80edec8934fe1617e87493d9e6b2e132ba69b570))
* **tweetCard:** more compact body text ([1876651](https://gitlab.com/karen/terrence/commit/1876651027e28ee279ab6fc165c276d7af11918a))
* **tweetgenerator:** random tweet should be idempotent per turn ([94536e6](https://gitlab.com/karen/terrence/commit/94536e602fe8ad919b9521466f65baa704669708))
* **tweetGenerator:** avoid duplicate news ([a2f5881](https://gitlab.com/karen/terrence/commit/a2f5881a80014deb1faf26325e1864c22ed394b5))
* **UI:** added some latency to see elements better ([31738e1](https://gitlab.com/karen/terrence/commit/31738e13d0c98887caabb66ca3b6bb3adf88ee09))
* **UI:** colors update ([e6f44a6](https://gitlab.com/karen/terrence/commit/e6f44a6f58e968997765a865eeb1a714b37c2634))
* **writing:** stocks card update ([181b5c5](https://gitlab.com/karen/terrence/commit/181b5c5944eb142e4b51394507c55cfd1d181ea0))
* ignore runs and reports ([a4c1e41](https://gitlab.com/karen/terrence/commit/a4c1e41a46a7d0f535d4d8fe6c4489715a5982ab))
* move timeline logic to game ui state ([95b97ef](https://gitlab.com/karen/terrence/commit/95b97ef5dee9b61f9b3beb2a5bda1e788ee827be))
* new formatters ([9cc43b4](https://gitlab.com/karen/terrence/commit/9cc43b4b1ffb5b88544a12b46dbbce4b31d2a040))
* remove cra mentions ([4a558a1](https://gitlab.com/karen/terrence/commit/4a558a1e07038dd06af84b4cd84593ad8c2ab8f9))
* **uistate:** reset tweet list ([a2a554e](https://gitlab.com/karen/terrence/commit/a2a554effd662fd187e9f00768e31b1295b79de0))
* **win:** display time spent and final population ([f0e896b](https://gitlab.com/karen/terrence/commit/f0e896b91a8c962c497f1f61e92c1b443eb8f9bc))
* remove useless assets ([b4dc4b6](https://gitlab.com/karen/terrence/commit/b4dc4b63936ccfa723bb93993534557e7f02de65))
* tweak growth steps ([15b17d2](https://gitlab.com/karen/terrence/commit/15b17d22e8bce698f21e3e196d1d1e35fec28636))
* tweak intro action hints ([7635cef](https://gitlab.com/karen/terrence/commit/7635cefe790770ced4977f7c1cba76aa89d68eb9))
* unused classnames import ([e86a40a](https://gitlab.com/karen/terrence/commit/e86a40a0b3d8fa883c0d2f1e9059b6080b16d845))
