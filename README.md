![Citerate banner](resources/other/00Bis_Banner.png)

Citerate is a city simulation game, inspired by Reigns and other city simulation.

The objective is to develop a new city in a iterative way, rather than in a master plan one.

Want to learn more and exchange about the game, join us on the Discord [Citerate club](https://discord.gg/2J9ATWz)

## Libraries / tools in this project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

- react
- [react-swipeable](https://github.com/FormidableLabs/react-swipeable)
- [Tailwind CSS](https://tailwindcss.com/)

## How to contribute

See [the contribution guide](https://gitlab.com/karen/terrence/-/blob/master/CONTRIBUTING.md).
