# Contributing

Thank you for considering helping our project ❤️

There are multiple ways you can contribute to this game:

- with bug reports
- with code
- with game design or elements

## Contributing bug reports 🐞

At the heart of every project are bugs, issues and other inconsistencies. If you find anything that doesn't feel right, please [fill in a new issue](https://gitlab.com/karen/terrence/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

## Contributing code 👩‍💻

The game is made using web technologies. More specifically it is based on React.

Fork and open new MR's ⭐️

## Contributing game elements

You can update the configuration of the game:

- for updating infrastructure types metadata [infra-categories.json](https://gitlab.com/karen/terrence/-/blob/master/src/config/data/infra-categories.json)
- for updating infrastructure buildings [infra-card-definitions.json](https://gitlab.com/karen/terrence/-/blob/master/src/config/data/infra-card-definitions.json)
- for the winning conditions [goals.js](https://gitlab.com/karen/terrence/-/blob/master/src/config/goals.js)
- for the initial game values [state.js](https://gitlab.com/karen/terrence/-/blob/master/src/config/state.js)

If you want to update the game more deeply you can check [the state manager](https://gitlab.com/karen/terrence/-/blob/master/src/hooks/gamestate/index.js).

Of course, you can offer the changes directly through MR's 🚀
