import React from "react";
import ReactDOM from "react-dom";

import "./assets/main.css";

import App from "./App";

import { UIConfigProvider } from "hooks/config";
import { initAmplitude } from "services/amplitude";

function isPhoneGap() {
  return (
    (window.cordova || window.PhoneGap || window.phonegap) &&
    /^file:\/{3}[^/]/i.test(window.location.href) &&
    /ios|iphone|ipod|ipad|android/i.test(navigator.userAgent)
  );
}

function isPWAContext() {
  // replace standalone with fullscreen or minimal-ui according to your manifest
  if (matchMedia("(display-mode: standalone)").matches) {
    // Android and iOS 11.3+
    return true;
  } else if ("standalone" in navigator) {
    // useful for iOS < 11.3
    return Boolean(navigator.standalone);
  }
  return false;
}

if (isPhoneGap()) {
  console.log("Running in a Cordova context");
  document.addEventListener(
    "deviceready",
    () => {
      window.open = window.cordova.InAppBrowser.open;
      initApp(true);
    },
    false
  );
  initAmplitude("app");
} else {
  console.log("Running in a web context");
  initAmplitude(isPWAContext() ? "pwa" : "web");
  initApp(isPWAContext());
}

window.addEventListener("error", (event) => {
  console.log("An error occured, restarting the app");
  initApp(isPWAContext(), event);
});

function initApp(isAppContext, errorEvent) {
  ReactDOM.render(
    <React.StrictMode>
      <UIConfigProvider>
        <App isAppContext={isAppContext} errorEvent={errorEvent} />
      </UIConfigProvider>
    </React.StrictMode>,
    document.getElementById("root")
  );
}
