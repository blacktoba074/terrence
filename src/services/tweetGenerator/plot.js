import { getMessage, getNewPlots } from "config/plots";
import { getCityServiceLevel } from "services/state";
import { makeTweet, makeAnswerTweet } from "./factory";

const publicURL = process.env.PUBLIC_URL;

const isPlotTweetNext = (gamestate) => {
  const occupiedPlots = gamestate.population / 2;
  const getLucky = Math.random() > 0.8;
  return (
    gamestate.tweet &&
    (gamestate.tweet.type === "infra" ||
      gamestate.tweet.type === "intro" ||
      gamestate.tweet.type === "event" ||
      gamestate.tweet.type === "stocks") &&
    (occupiedPlots >= 0.8 * gamestate.plotNumber || getLucky)
  );
};

const getPlotSaleTweet = (gamestate) => {
  const serviceLevel = 1 + getCityServiceLevel(gamestate);
  const cardVersion = Math.random() < 0.5 ? "" : "-B";
  const cardLevel = Math.min(serviceLevel, 5);

  const newPlots = getNewPlots(gamestate);

  return makeTweet({
    type: "plot",
    author: "@realEstateDaily",
    message: getMessage(gamestate),
    impact: {
      newPlots,
      cost: 0,
    },
    illustrationPath: `${publicURL}/other-cards/Expand_0${cardLevel}${cardVersion}.png`,
  });
};

const getPlotAnswer = (action, tweet) => {
  const message =
    action === "right"
      ? "Let's expand the city!"
      : "The city is big enough for the moment.";
  return makeAnswerTweet(message, tweet);
};

export { isPlotTweetNext, getPlotSaleTweet, getPlotAnswer };
