import { getCurrentThreshold } from "config/contracts";

const { makeTweet } = require("./factory");

const publicURL = process.env.PUBLIC_URL;

const isNextTweetAContractUpdate = (gamestate) => {
  // The tweet in the game state is the last one because we are making the next
  const lastTweet = gamestate.tweet;
  return (
    (lastTweet.type === "plot" || lastTweet.type === "infra") &&
    getCurrentThreshold(gamestate) !== undefined
  );
};

const tweets = [
  "No one wants to come yet. Maybe we should give some ownership to the first citizens?",
  "Maybe we can stop giving part of the city and just provide the plots for free?",
  "What about making money from the new plot sales?",
];

const getContractUpdateTweet = (gamestate) => {
  const currentThreshold = getCurrentThreshold(gamestate);
  const message = tweets[currentThreshold];

  return makeTweet({
    type: "contract",
    author: "@landOwner2",
    message,
    impact: {
      contractLevel: currentThreshold + 1,
    },
    actionHints: {
      left: "No",
      right: "Yes!",
    },
    illustrationPath: `${publicURL}/other-cards/StockGiveaway.png`,
  });
};

const getContractUpdateAnswer = (gamestate) => ({
  left: "Our current contract strategy is good for the moment.",
  right: "Let's update the terms",
});

export {
  isNextTweetAContractUpdate,
  getContractUpdateAnswer,
  getContractUpdateTweet,
};
