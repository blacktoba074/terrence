let lastTweetId = 0;

export const makeTweet = (content) => ({
  ...content,
  id: ++lastTweetId,
});

export const makeAnswerTweet = (
  message,
  originalTweet,
  author = "@newCityCorpCEO"
) =>
  makeTweet({
    type: "answer",
    author,
    message,
    originalTweet,
  });
