const growthSteps = [100, 10000, 500000];

export function passedGrowthStep(previousPopulation, currentPopulation) {
  return Boolean(
    growthSteps.find((s) => previousPopulation > s && currentPopulation > s)
  );
}
