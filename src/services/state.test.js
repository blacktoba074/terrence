import * as stateServices from "./state";

describe("getCityServiceLevel", () => {
  it("returns the city service level 0 when there is no service", () => {
    const gamestate = {};
    expect(stateServices.getCityServiceLevel(gamestate)).toBe(0);
  });

  it("returns the max of different service levels, count from 0", () => {
    const gamestate = {
      availableServices: ["SharedHotspot", "Wells"],
    };
    expect(stateServices.getCityServiceLevel(gamestate)).toBe(0);
  });

  it("returns the max of different service levels", () => {
    const gamestate = {
      availableServices: [
        "SharedHotspot",
        "Wells",
        "FirstAidKit",
        "IndividualGenerator",
        "UnpavedRoads",
        "FireExtinguishers",
      ],
    };
    expect(stateServices.getCityServiceLevel(gamestate)).toBe(1);
  });

  it("returns the next level if the max is complete", () => {
    const gamestate = {
      availableServices: [
        "SharedHotspot",
        "Wells",
        "FirstAidKit",
        "IndividualGenerator",
      ],
    };
    expect(stateServices.getCityServiceLevel(gamestate)).toBe(1);
  });
});
