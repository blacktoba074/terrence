import React, { useRef } from "react";

import { useConfig } from "hooks/config";
import useGameState from "hooks/gamestate";
import useGameUIState from "hooks/gameUIState";
import useBoundingRect from "hooks/useBoundingRect";
import Layout from "components/layout";
import DebugModal from "components/modals/debug";
import StatsModal from "components/modals/stats";
import GameOver from "components/modals/gameover";
import Intro from "components/modals/intro";
import PickACityName from "components/modals/pickname";
import Win from "components/modals/win";
import GoalReachedModal from "components/modals/goal";
import { UpdatePopup } from "components/popups/update";
import { ErrorPopup } from "components/popups/error";
import { MainGameScreen } from "./main";

const Game = ({ workerState, errorEvent, isAppContext }) => {
  const { config } = useConfig();
  const containerRef = useRef(undefined);
  const height = useBoundingRect(containerRef);

  const {
    left,
    right,
    quit,
    state: gamestate,
    history,
    reset,
    isWin,
    setCityName,
  } = useGameState();

  const tweet = gamestate.tweet;

  // UI state
  const {
    state: gameUIState,

    setChosenAction,
    toggleDebug,
    toggleStatsModal,
    toggleReachGoalModal,
    hideNewTurn,
    hideIntro,
    hideNewLevel,
    nextTweet,
    quitGame,
    selectCityName,

    tweetQueue,
  } = useGameUIState({
    gamestate,
    history,
    left,
    right,
    quit,
    config,
    setCityName,
  });

  return (
    <Layout
      onClick={(e) => {
        // triggers debug screen if tapped 3 times in a row
        if (e.detail === 3) toggleDebug();
      }}
      gamestate={gamestate}
      ref={containerRef}
    >
      <PickACityName
        show={gameUIState.showPickCityName}
        selectCityName={selectCityName}
        gamestate={gamestate}
      />

      <Intro
        show={gameUIState.showIntroScreen}
        onConfirm={hideIntro}
        gamestate={gamestate}
      />

      {!gameUIState.showIntroScreen && !gameUIState.showPickCityName && (
        <MainGameScreen
          gameUIState={gameUIState}
          gamestate={gamestate}
          history={history}
          hideNewLevel={hideNewLevel}
          hideNewTurn={hideNewTurn}
          setChosenAction={setChosenAction}
          toggleStatsModal={toggleStatsModal}
          tweet={tweet}
          nextTweet={nextTweet}
          height={height}
        />
      )}

      <DebugModal
        show={gameUIState.displayDebug}
        toggleDebug={toggleDebug}
        gamestate={gamestate}
        gameUIState={gameUIState}
        tweetQueue={tweetQueue}
      ></DebugModal>

      <StatsModal
        show={gameUIState.showStatsModal}
        toggleStatsModal={toggleStatsModal}
        gameUIState={gameUIState}
        gamestate={gamestate}
        history={history}
        quitGame={quitGame}
      />

      <GoalReachedModal
        gamestate={gamestate}
        show={gameUIState.showReachGoalModal}
        hideScreen={toggleReachGoalModal}
      />

      <GameOver
        show={gameUIState.showGameOverScreen}
        reset={reset}
        gamestate={gamestate}
      />

      <Win reset={reset} show={isWin} gamestate={gamestate} history={history} />

      <UpdatePopup {...workerState} />
      <ErrorPopup errorEvent={errorEvent} isAppContext={isAppContext} />
    </Layout>
  );
};

export default Game;
