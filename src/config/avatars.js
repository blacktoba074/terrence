const publicURL = process.env.PUBLIC_URL;

const getId = (citizenName) => {
  return parseFloat(citizenName.replace(/.*(\d+)/, "$1")) || 0;
};

const avatarDef = {
  "@newCityCorpCEO": {
    path: `${publicURL}/avatars/Avatar_C.png`,
    description: `The new city official account`,
  },
  "@thelocal": {
    path: `${publicURL}/avatars/Avatar_LocalNews.png`,
    description: `The local newspaper avatar`,
  },
  "@realEstateDaily": {
    path: `${publicURL}/avatars/Avatar_RealEstate.png`,
    description: `The local newspaper avatar`,
  },
  "@landOwner1": {
    path: `${publicURL}/avatars/Avatar_LandOwner1.png`,
    description: `A land owner willing to sell`,
  },
  "@landOwner2": {
    path: `${publicURL}/avatars/Avatar_LandOwner2.png`,
    description: `Another land owner`,
  },
  "@angel1": {
    path: `${publicURL}/avatars/Avatar_AngelInvestor.png`,
    description: `An angel investor`,
  },
  "@angel2": {
    path: `${publicURL}/avatars/Avatar_AngelInvestor.png`,
    description: `An angel investor`,
  },
  "@angel3": {
    path: `${publicURL}/avatars/Avatar_AngelInvestor.png`,
    description: `An angel investor`,
  },
};

// the numbers of avatars available for citizens
const nbAvatars = 36;

export const getAvatar = (author) => {
  if (avatarDef[author]) return avatarDef[author];

  const id = getId(author);
  return {
    path: `${publicURL}/avatars/avatar_${
      1 + ((7 + id * 3931) % nbAvatars)
    }.png`,
    description: `Portrait of ${author}`,
  };
};
