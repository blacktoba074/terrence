// Array of contracts
// Contracts takes the gamestate and returns an impact
const contracts = [
  {
    appeal: 0,
    ownership: 0,
    price: 0,
    minPopThreshold: 0,
  },
  {
    appeal: 0.7,
    ownership: 0.01,
    price: 0,
    minPopThreshold: 50,
  },
  {
    appeal: 0.4,
    ownership: 0,
    price: 0,
    minPopThreshold: 200,
  },
  {
    appeal: 0.05,
    ownership: 0,
    price: "market",
    minPopThreshold: Infinity,
  },
];

const maxLevel = contracts.length - 1;

const getCurrentContract = (state) => {
  const { contractLevel } = state;
  return getContractAtLevel(contractLevel);
};

const getContractAtLevel = (contractLevel) => {
  return contracts[Math.min(contractLevel, maxLevel)];
};

/**
 * Returns a
 * @returns {number}
 */
const getCurrentThreshold = (gamestate) => {
  const { contractLevel, population } = gamestate;

  const contractId = contracts.findIndex((c, i) => {
    console.log(c);
    return population >= c.minPopThreshold && contractLevel === i;
  });

  return contractId === -1 ? undefined : contractId;
};

export { getCurrentContract, getCurrentThreshold, getContractAtLevel };
