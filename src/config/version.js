import packageFile from "../../package.json";

const envSuffix =
  process.env.NODE_ENV === "production" ? "" : `-${process.env.NODE_ENV}`;

const getVersion = () =>
  packageFile ? `${packageFile.version}${envSuffix}` : `unknown${envSuffix}`;

export { getVersion };
