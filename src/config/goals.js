export default {
  population: 1000000,
  turns: 100,
  minHappiness: 0,
  minBankBalance: 0,
};
