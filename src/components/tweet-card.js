import React, { useRef, forwardRef } from "react";
import { Repeat, FavoriteBorder } from "@material-ui/icons";
import { animated } from "react-spring";

import { getAvatar } from "config/avatars";

const Avatar = ({ author }) => {
  const avatarDef = getAvatar(author);

  return (
    <div className="relative flex items-center justify-center w-12 h-12 rounded-full">
      <img
        className="rounded-full transform scale-125"
        src={avatarDef.path}
        alt={avatarDef.description}
      />
    </div>
  );
};

const TweetStats = ({ nbLikes, isLiked, fontSize }) => {
  return (
    <div className="flex flex-row text-xs">
      <div>
        <Repeat style={{ fontSize }} /> 20
      </div>
      <div className="ml-4">
        <FavoriteBorder style={{ fontSize }} />{" "}
        {`${nbLikes.current + (isLiked ? 1 : 0)}`}
      </div>
    </div>
  );
};

const TweetCard = forwardRef(({ tweet, isLiked = false, style }, ref) => {
  const nbLikes = useRef(Math.floor(Math.random() * 10 + 40));
  return (
    <animated.div style={style}>
      <div
        className="flex items-center justify-center w-full px-1 pb-1 bg-citerate-sand-yellow border border-orange-100 xs:px-2 xs:py-1 shadow-xs"
        ref={ref}
      >
        {tweet.type === "answer" && <div className="w-8"></div>}
        <div className="flex h-full mt-1 mb-auto">
          <Avatar author={tweet.author} />
        </div>
        <div className="flex flex-col items-start flex-grow h-full ml-2">
          <div className="text-xs font-light xs:font-semibold">
            {tweet.author}
          </div>
          <div className="text-xs leading-tight xs:text-sm leading-4">
            {tweet.message}
          </div>
          <div className="hidden xs:block">
            <TweetStats fontSize={14} nbLikes={nbLikes} isLiked={isLiked} />
          </div>
          <div className="block xs:hidden">
            <TweetStats fontSize={12} nbLikes={nbLikes} isLiked={isLiked} />
          </div>
        </div>
      </div>
    </animated.div>
  );
});

export default TweetCard;
