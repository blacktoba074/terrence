import React from "react";
import classnames from "classnames";

const displayedValues = [
  "population",
  "bankBalance",
  "happiness",
  // "plotPrice",
  //   "plotSales",
  // "plotNumber",
  // "taxIncome",
  "maintenance",
  //   "investment",
];

const DataPoint = ({ k, value, previousValue, isFirstTurn }) => {
  const delta = value - previousValue;
  return (
    <div className="flex flex-row flex-grow w-full border-t first:border-t-0">
      <div className="flex-grow">{k}</div>
      <div>{Math.floor(value)}</div>
      {!isFirstTurn && (
        <div
          className={classnames({
            "text-red-500": delta < 0,
            "text-green-700": delta > 0,
          })}
        >
          ({delta > 0 && "+"}
          {delta.toFixed(2)})
        </div>
      )}
    </div>
  );
};

const Header = ({ gamestate, history }) => {
  const isFirstTurn = gamestate.turn === 0;
  const previousState = isFirstTurn ? gamestate : history[history.length - 1];
  const keys = [...Object.keys(gamestate)];

  return (
    <div className="flex flex-row flex-wrap text-xs border p-1 w-full">
      {keys
        .filter(
          (k) =>
            displayedValues.indexOf(k) !== -1 &&
            typeof gamestate[k] === "number"
        )
        .map((k) => (
          <DataPoint
            key={k}
            k={k}
            value={gamestate[k]}
            previousValue={previousState[k]}
            isFirstTurn={isFirstTurn}
          />
        ))}
    </div>
  );
};

export default Header;
