import React from "react";
import classnames from "classnames";

import { ReactComponent as MadIcon } from "assets/art/MadIcon.svg";
import { ReactComponent as HappyIcon } from "assets/art/HappyIcon.svg";

const Popularity = ({ className, gamestate, ...otherProps }) => (
  <div className="absolute top-0 left-0 bottom-0 flex flex-col h-full ml-1 w-8">
    <HappyIcon className="mx-auto pb-2" />
    <div
      className={classnames(
        "relative w-2 flex-grow top-0 left-0 mx-auto",
        className
      )}
      {...otherProps}
    >
      <div className="absolute top-0 left-0 bottom-0 w-full h-full bg-blue-400 rounded-full"></div>
      <div
        className="absolute bottom-0 w-full bg-blue-800 rounded-full"
        style={{ height: `${gamestate.happiness}%` }}
      ></div>
    </div>
    <MadIcon className="mx-auto pt-2" />
  </div>
);

export default Popularity;
