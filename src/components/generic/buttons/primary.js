import React from "react";
import classnames from "classnames";
import BaseButton from "./base";

const PrimaryButton = ({
  className,
  children,
  isTitleScreen = false,
  ...otherProps
}) => (
  <BaseButton
    className={classnames(
      "rounded focus:outline-none focus:shadow-outline mx-auto text-center px-4 py-2 w-full font-citerate font-semibold text-xl",
      {
        "bg-white text-citerate-deep-green": isTitleScreen,
        "bg-citerate-grass-green text-citerate-deep-purple": !isTitleScreen,
      },
      className
    )}
    {...otherProps}
  >
    {children}
  </BaseButton>
);

export default PrimaryButton;
