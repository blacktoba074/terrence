import React from "react";
const ExternalLink = (props) => (
  // eslint-disable-next-line
  <a
    className="text-blue-700 underline"
    href="https://twitter.com/2xgdp"
    rel="noopener noreferrer"
    target="_blank"
    {...props}
  />
);

export default ExternalLink;
