import React from "react";
import classnames from "classnames";

const ButtonLinkCordova = ({ className, href, ...otherProps }) => (
  <button
    className={classnames(
      "block bg-pink-200 w-full text-gray-900 rounded focus:outline-none focus:shadow-outline mx-auto text-center px-4 py-2",
      className
    )}
    onClick={() => {
      console.log("Opening page:", href);
      window.open(href, "_system");
    }}
    {...otherProps}
  />
);

const ButtonLinkWeb = ({ className, children, ...otherProps }) => (
  <a
    className={classnames(
      "block bg-pink-200 w-full text-gray-900 rounded focus:outline-none focus:shadow-outline mx-auto text-center px-4 py-2",
      className
    )}
    rel="noreferrer noopener"
    target="_blank"
    {...otherProps}
  >
    {children}
  </a>
);

const ButtonLink = (props) =>
  typeof window.cordova === "object" ? (
    <ButtonLinkCordova {...props} />
  ) : (
    <ButtonLinkWeb {...props} />
  );

export default ButtonLink;
