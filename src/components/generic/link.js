import React from "react";
import BaseButton from "components/generic/buttons/base";

const Link = (props) => (
  <BaseButton className="underline" {...props}></BaseButton>
);

export default Link;
