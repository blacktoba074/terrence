import infraCategories from "config/data/infra-categories.json";

const rainbowColor = infraCategories.map((c) => c.color);
const rainbowAccent = infraCategories.map((c) => c.accent);

export { rainbowColor, rainbowAccent };
