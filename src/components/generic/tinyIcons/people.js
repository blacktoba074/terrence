import * as React from "react";
import classnames from "classnames";

function PeopleIcon({ className, ...props }) {
  return (
    <svg
      viewBox="0 0 150 150"
      className={classnames("w-full h-full", className)}
      {...props}
    >
      <style>{".prefix__st32{fill:#462e53}"}</style>
      <g>
        <circle className="prefix__st32" cx={56.58} cy={57.01} r={22.43} />
        <path
          className="prefix__st32"
          d="M89.3 115.42c.43-2.13.65-4.33.65-6.58 0-18.43-14.94-33.37-33.37-33.37s-33.37 14.94-33.37 33.37c0 2.25.23 4.45.65 6.58H89.3z"
        />
        <circle className="prefix__st32" cx={101.87} cy={71.82} r={16.74} />
        <path
          className="prefix__st32"
          d="M126.3 115.42c.32-1.59.49-3.23.49-4.91 0-13.76-11.15-24.91-24.91-24.91s-24.91 11.15-24.91 24.91c0 1.68.17 3.32.49 4.91h48.84z"
        />
      </g>
    </svg>
  );
}

export default PeopleIcon;
