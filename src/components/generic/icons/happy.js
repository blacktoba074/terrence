import React from "react";
import classnames from "classnames";

const HappyIcon = ({ className }) => (
  <svg
    className={classnames("h-full", className)}
    viewBox="0 0 25 25"
    fill="currentColor"
    xmlns="http://www.w3.org/2000/svg"
    style={{
      fillRule: "evenodd",
      clipRule: "evenodd",
      strokeLinejoin: "round",
      strokeMiterlimit: 2,
    }}
  >
    <g transform="matrix(0.92,0,0,0.92,1,1)">
      <path d="M12.5,25C19.404,25 25,19.404 25,12.5C25,5.596 19.404,0 12.5,0C5.596,0 0,5.596 0,12.5C0,19.404 5.596,25 12.5,25ZM20.699,12.568C20.699,17.097 17.027,20.767 12.5,20.767C7.973,20.767 4.301,17.095 4.301,12.568C4.301,11.323 20.699,11.441 20.699,12.568ZM8.254,5.207C9.483,5.207 10.481,6.205 10.481,7.434C10.481,8.663 9.483,9.661 8.254,9.661C7.024,9.661 6.026,8.663 6.026,7.434C6.026,6.205 7.024,5.207 8.254,5.207ZM16.746,5.207C17.976,5.207 18.974,6.205 18.974,7.434C18.974,8.663 17.976,9.661 16.746,9.661C15.517,9.661 14.519,8.663 14.519,7.434C14.519,6.205 15.517,5.207 16.746,5.207Z" />
    </g>
  </svg>
);

export default HappyIcon;
