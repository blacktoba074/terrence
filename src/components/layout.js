import React, { useLayoutEffect, useRef } from "react";
import classnames from "classnames";
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock";
import { getCurrentEvent } from "services/state";
import { getEventDefinition } from "config/events";

const currentFilter = (gamestate) => {
  if (!gamestate) return "";

  const event = getCurrentEvent(gamestate);
  if (!event) return "";

  const eventDefinition = getEventDefinition(event);
  return eventDefinition.cssFilter || "";
};

const Layout = (
  {
    children,
    className = "bg-citerate-sky-blue",
    gamestate = null,
    ...otherProps
  },
  ref
) => {
  const layoutRootRef = useRef(undefined);
  useLayoutEffect(() => {
    if (ref) ref.current = layoutRootRef.current;
    const layoutRef = layoutRootRef.current;
    disableBodyScroll(layoutRef);
    return () => {
      enableBodyScroll(layoutRef);
    };
  });

  return (
    <div
      id="layout-root"
      className="w-full h-full min-h-full md:py-8 pattern "
      style={{
        filter: currentFilter(gamestate),
      }}
    >
      <div
        className={classnames(
          "w-full sm:max-w-sm mx-auto sm:max-h-sm md:shadow-lg overflow-hidden h-full flex flex-col items-center justify-start relative",
          className
        )}
        {...otherProps}
        ref={layoutRootRef}
      >
        {children}
      </div>
    </div>
  );
};

export default React.forwardRef(Layout);
