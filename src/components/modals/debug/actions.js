import { SecondaryButton } from "components/generic/buttons";
import React from "react";

export const ActionsPanel = () => {
  return (
    <div className="p-4">
      <SecondaryButton
        onClick={() => {
          window.crashMePlease();
        }}
        className="mt-4"
      >
        Crash the app
      </SecondaryButton>
    </div>
  );
};
