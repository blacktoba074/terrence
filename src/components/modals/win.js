import React from "react";
import classnames from "classnames";

import Modal from "components/generic/modal";

import { PrimaryButton } from "components/generic/buttons";

import {
  abbreviate,
  abbreviateMoney,
  formatInteger,
  formatPercent,
} from "services/format";

import CalendarIcon from "components/generic/tinyIcons/calendar";
import HomeIcon from "components/generic/tinyIcons/plots";
import HappyIcon from "components/generic/tinyIcons/happiness";
import CurrencyIcon from "components/generic/tinyIcons/cash";
import ModalHeader from "components/generic/modal-header";
import StarIcon from "components/generic/tinyIcons/star";

import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import LinkedInIcon from "@material-ui/icons/LinkedIn";

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
} from "react-share";
import { getCityServiceLevel } from "services/state";

const sharedURL = "https://citerate.com";

const P = ({ className, ...props }) => (
  <p {...props} className={classnames("text-base mx-auto", className)} />
);

const Grid = (props) => (
  <div className="flex-grow my-4 grid grid-cols-3 gap-2" {...props} />
);
const Separator = (props) => <hr className="col-span-3" {...props} />;
const Icon = (props) => <div className="w-10 h-10 mx-auto" {...props} />;
const Value = (props) => (
  <div
    className="my-auto text-lg font-bold text-right md:text-2xl"
    {...props}
  />
);
const Label = (props) => (
  <div className="my-auto text-sm sm:text-base" {...props} />
);

/**
 * @param history {Array} all gamestates
 */
const computeStats = (gamestate, history) => {
  const averageHappiness =
    (gamestate.happiness + history.reduce((m, gs) => m + gs.happiness, 0)) /
    (history.length + 1);
  const totalIncome = history.reduce(
    (m, gs) => m + gs.taxIncome + gs.plotSales,
    0
  );
  const duration = Math.floor(gamestate.turn / 4);
  const cityLevel = getCityServiceLevel(gamestate);

  return {
    population: gamestate.population,
    totalIncome,
    duration,
    cityLevel,
    averageHappiness,
  };
};

const Win = ({ show, reset, gamestate, history }) => {
  const {
    population,
    totalIncome,
    duration,
    cityLevel,
    averageHappiness,
  } = computeStats(gamestate, history);
  const message = `This time ${
    gamestate.cityName
  } reached a population of ${abbreviate(gamestate.population, 4)} people. 
Can you beat that?`;

  return (
    <Modal show={show} fullHeight>
      <div className="flex flex-col justify-items-center">
        <ModalHeader className="mx-auto text-2xl">Woohoo!</ModalHeader>
        <P>{gamestate.cityName} results</P>
      </div>

      <Grid>
        <Icon>
          <HappyIcon happiness="0.7" />
        </Icon>
        <Value>{formatPercent(averageHappiness * 0.01)}</Value>
        <Label>Average happiness</Label>

        <Separator />

        <Icon>
          <HomeIcon />
        </Icon>
        <Value>{abbreviate(population)}</Value>
        <Label>Population</Label>

        <Separator />

        <Icon>
          <CurrencyIcon />
        </Icon>
        <Value>{abbreviateMoney(totalIncome)}</Value>
        <Label>Total income</Label>

        <Separator />

        <Icon>
          <CalendarIcon />
        </Icon>
        <Value>{formatInteger(duration)}</Value>
        <Label>Years</Label>

        <Separator />

        <Icon>
          <StarIcon className="w-10" />
        </Icon>
        <Value>{formatInteger(cityLevel)}</Value>
        <Label>City level</Label>
      </Grid>

      <div className="flex flex-col w-full text-white">
        <p className="m-auto font-light uppercase">Share your results</p>
        <div className="flex flex-row h-8 mx-auto to-citerate-deep-purple">
          <TwitterShareButton
            url={sharedURL}
            title={`${message}

@citerateGame #citerate #buildingcities #citysim`}
          >
            <TwitterIcon />
          </TwitterShareButton>
          <FacebookShareButton
            url={sharedURL}
            quote={`${message} citerategame`}
            hashtag="#citerate"
            className="mx-4"
          >
            <FacebookIcon />
          </FacebookShareButton>
          <LinkedinShareButton
            url={sharedURL}
            title="A new city is born!"
            summary={message}
            source="Citerate"
          >
            <LinkedInIcon />
          </LinkedinShareButton>
        </div>
      </div>

      <PrimaryButton onClick={reset} className="mt-4">
        Start a new city
      </PrimaryButton>
    </Modal>
  );
};

export default Win;
