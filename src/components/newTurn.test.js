const { populationScale } = require("./newTurn");

describe("Population scale", () => {
  it.each`
    population | progress
    ${0}       | ${0}
    ${1}       | ${0}
    ${10}      | ${100 / 6}
    ${100}     | ${100 / 3}
  `(
    "should return $progress when population is $population",
    ({ population, progress }) => {
      expect(populationScale(population)).toBe(progress);
    }
  );
});
