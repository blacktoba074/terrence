import React, { useEffect } from "react";
import { animated, config, useSprings } from "react-spring";
import classnames from "classnames";
import HeartEyes from "components/generic/sparkles/hearteyes";

const toPercentString = (v) => `${v * 100}%`;

const LoveWall = ({ show, startY = 1, endY = -0.5 }) => {
  const [springs, set] = useSprings(10, () => ({
    from: {
      top: toPercentString(startY + Math.random() * 0.05),
      left: toPercentString(Math.random() * 0.5 + 0.25),
    },
    config: {
      ...config.molasses,
      friction: config.slow.friction * (1 + Math.random() * 0.5),
    },
    duration: 500,
  }));

  useEffect(() => {
    if (show)
      set(() => ({
        to: {
          top: toPercentString(endY),
          left: toPercentString(Math.random()),
        },
        reset: true,
      }));
  }, [set, show, endY]);

  return (
    <div className="absolute top-0 left-0 w-full h-full pointer-events-none z-100">
      {springs.map((props, i) => (
        <animated.div
          key={`${i}-love`}
          className={classnames("absolute w-10 h-10", {
            "opacity-100": show,
            "opacity-0": !show,
          })}
          style={props}
        >
          <HeartEyes className="h-full w-full" />
        </animated.div>
      ))}
    </div>
  );
};

export default LoveWall;
