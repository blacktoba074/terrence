import React, { useRef, useState } from "react";
import { PrimaryButton } from "components/generic/buttons";
import CancelIcon from "@material-ui/icons/Cancel";
import { useSpring, animated, config } from "react-spring";

export const UpdatePopup = ({
  /* serviceWorkerInitialized = false, */
  serviceWorkerUpdated = false,
  reload = null,
}) => {
  const firstLoad = useRef(true);
  const [isPopupDiscarded, setPopupDiscarded] = useState(false);
  const popupProps = useSpring({
    from: { position: !serviceWorkerUpdated || isPopupDiscarded ? 1 : 0 },
    to: { position: !serviceWorkerUpdated || isPopupDiscarded ? 0 : 1 },
    immediate: firstLoad.current,
    config: config.wobbly,
  });

  firstLoad.current = false;

  return (
    <animated.div
      className="absolute bottom-0 left-0 w-full z-100"
      style={{
        transform: popupProps.position.interpolate(
          (p) => `translateY(${(0.9 - p) * 120}%)`
        ),
      }}
    >
      <div className="max-w-xs mx-auto bg-citerate-sand-yellow p-4 ">
        <div className="flex flex-row mb-4 ">
          <div className="flex-grow text-xl">A new version is here!</div>
          <button>
            <CancelIcon
              onClick={() => {
                setPopupDiscarded(true);
              }}
            />
          </button>
        </div>
        <PrimaryButton onClick={reload}>Update now</PrimaryButton>
      </div>
    </animated.div>
  );
};
