import React, { useEffect, useMemo, useRef, useState } from "react";
import { PrimaryButton, SecondaryButton } from "components/generic/buttons";
import { useSpring, animated, config } from "react-spring";
import { getVersion } from "config/version";

const makeMessage = (error, isAppContext) => {
  return `
---------------------------------------
Thanks for helping! Please describe what happened above.
If you don't have anything to add, just hit "send"!

 - Version: ${getVersion()}
 - Context: ${isAppContext ? "App" : "Web"}
 - Time: ${new Date()}
 - Error:
\`\`\`
${error.stack}
\`\`\`
  `;
};

export const ErrorPopup = ({ errorEvent, isAppContext }) => {
  const firstLoad = useRef(true);
  const [showPopup, setShowPopup] = useState(false);
  const popupProps = useSpring({
    from: { position: showPopup ? 0 : 1 },
    to: { position: showPopup ? 1 : 0 },
    immediate: firstLoad.current,
    config: config.stiff,
  });

  const message = useMemo(() => {
    return errorEvent ? makeMessage(errorEvent.error, isAppContext) : "";
  }, [errorEvent, isAppContext]);

  useEffect(() => {
    setShowPopup(Boolean(errorEvent));
  }, [setShowPopup, errorEvent]);

  firstLoad.current = false;

  return (
    <animated.div
      className="absolute bottom-0 left-0 h-full w-full bg-black bg-opacity-25 z-100"
      style={{
        display: popupProps.position.interpolate((p) =>
          p < 0.01 ? "none" : "block"
        ),
      }}
    >
      <animated.div
        className="absolute bottom-0 left-0 w-full h-full flex justify-center items-center"
        style={{
          transform: popupProps.position.interpolate(
            (p) => `translateY(${(0.9 - p) * 120}%)`
          ),
        }}
      >
        <div className="max-w-xs p-4 mx-auto bg-citerate-sand-yellow flex flex-col">
          <div className="flex flex-row mb-4 ">
            <div className="flex-grow text-xl">
              (⩾﹏⩽) the game just crashed. Sorry about that.
            </div>
          </div>
          <PrimaryButton
            href={encodeURI(
              `mailto:incoming+karen-terrence-19244005-issue-@incoming.gitlab.com?subject=App crash&body=${message}`
            )}
            onClick={() => {
              setShowPopup(false);
            }}
          >
            Report the bug
          </PrimaryButton>
          <SecondaryButton
            onClick={() => {
              setShowPopup(false);
            }}
          >
            Discard
          </SecondaryButton>
        </div>
      </animated.div>
    </animated.div>
  );
};
