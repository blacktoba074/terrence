import React, { useState } from "react";

import InfraCard from "./infra";
import StocksCard from "./stocks";
import GameOverCard from "./gameover";
import PlotCard from "./plot";
import IntroCard from "./intro";
import DebugCard from "./debug";
import ContractCard from "./contract";
import EventCard from "./event";

import LoveWall from "components/effects/lovewall";

const cardTypeMapping = new Map([
  ["infra", InfraCard],
  ["stocks", StocksCard],
  ["gameover", GameOverCard],
  ["plot", PlotCard],
  ["intro", IntroCard],
  ["contract", ContractCard],
  ["event", EventCard],
]);

const CardContainer = ({
  id,
  tweet,
  showCard,
  height,
  setChosenAction,
  gamestate,
}) => {
  const [showLove, setShowLove] = useState(false);
  const left = () => setChosenAction("left");
  const right = () => {
    if (
      tweet.type === "infra" ||
      (tweet.type === "intro" && gamestate.tick === 1)
    ) {
      setShowLove(tweet.category === tweet.messageCategory);
      setTimeout(() => {
        setChosenAction("right");
        setTimeout(() => setShowLove(false), 200);
      }, 300);
    } else {
      setChosenAction("right");
    }
  };

  const CardType = cardTypeMapping.has(tweet.type)
    ? cardTypeMapping.get(tweet.type)
    : DebugCard;

  return (
    <div className="relative top-0 left-0 flex-grow w-full mt-4" id={id}>
      <div className="absolute bottom-0 left-0 w-full h-full">
        <div className="h-full mx-auto">
          {showCard && (
            <CardType
              left={left}
              right={right}
              gamestate={gamestate}
              key={gamestate.tick}
              height={height}
            />
          )}
          <LoveWall show={showLove} />
        </div>
      </div>
    </div>
  );
};

export default CardContainer;
