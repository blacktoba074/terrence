import React from "react";

import GenericCard from "./generic-card";
import CardContent from "./content";

const ContractCard = ({ left, right, gamestate, height }) => {
  const tweet = gamestate.tweet;

  return (
    <GenericCard
      left={left}
      right={right}
      gamestate={gamestate}
      actionHints={tweet.actionHints}
      height={height}
    >
      <CardContent
        title="Plot buying terms"
        color="#E7FBD7"
        accent="#B7E184"
        illustrationPath={tweet.illustrationPath}
      >
        Price: {tweet.impact.contractLevel < 3 ? "No charge" : "Market price"}
        <br />
        City shares: {tweet.impact.contractLevel === 1 ? "1% per plot" : "None"}
      </CardContent>
    </GenericCard>
  );
};

export default ContractCard;
