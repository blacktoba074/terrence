import React, { useState, useRef, useEffect, useMemo } from "react";
import { useSwipeable } from "react-swipeable";
import classnames from "classnames";
import { getAnswer } from "services/tweetGenerator";

import {
  useSpring,
  animated,
  config as springConfig,
  interpolate,
} from "react-spring";
import TweetCard from "components/tweet-card";
import { useConfig } from "hooks/config";

const rotate = (swipeAngle, inverse = false) =>
  `rotate(${((inverse ? -1 : 1) * swipeAngle) / 15}deg)`;

const thresholds = [-60, 60];

const Card = ({ left, right, gamestate, actionHints, children, height }) => {
  const tweet = gamestate.tweet;

  const { config } = useConfig();

  const [uiState, setUistate] = useState({
    isSwiping: false,
    angle: 0,
  });

  const [isIdle, setIsIdle] = useState(false);
  const timeoutIdleRef = useRef(undefined);
  useEffect(() => {
    if (
      !isIdle &&
      !timeoutIdleRef.current &&
      gamestate.turn < 3 &&
      config.idleCardBounce
    ) {
      timeoutIdleRef.current = setTimeout(() => setIsIdle(true), 3000);
    }

    if (uiState.isSwiping) {
      setIsIdle(false);
      clearTimeout(timeoutIdleRef.current);
      timeoutIdleRef.current = undefined;
    }

    return () => {
      clearTimeout(timeoutIdleRef.current);
      timeoutIdleRef.current = undefined;
    };
  }, [isIdle, uiState.isSwiping, config.idleCardBounce, gamestate.turn]);

  const action = useRef(undefined);

  const cardProps = useSpring({
    from: {
      a: 0,
      swipe: 0,
    },
    config: isIdle ? springConfig.wobbly : springConfig.stiff,
    to: async (next) => {
      if (uiState.isSwiping || action.current) {
        await next({
          a: uiState.angle,
          swipe: uiState.isSwiping ? 1 : 0,
        });
      } else {
        if (!isIdle) {
          await next({
            swipe: 0,
          });
        } else {
          await next({ a: 120, velocity: 500 });
          await next({ a: -120, velocity: 500 });
          await next({ a: 0, velocity: 500 });
          await next({ a: 120, velocity: 500 });
          await next({ a: -120, velocity: 500 });
          await next({ a: 0, velocity: 500 });
          await next({ a: 120, velocity: 500 });
          await next({ a: -120, velocity: 500 });
          await next({ a: 0, velocity: 500 });
        }
      }
    },
  });

  const interpolatedTransform = interpolate(
    [cardProps.a, cardProps.swipe],
    (angle, swipe) =>
      `rotate(${angle / 15}deg) translateY(${swipe}px) scale(${
        swipe * 0.04 + 1.0
      })`
  );

  const interpolatedShadow = interpolate(
    [cardProps.swipe],
    (swipe) =>
      `0px 0px ${4 + swipe * 15}px 0px rgba(0,0,0,${0.2 + swipe * 0.3})`
  );

  const interpolatedZIndex = interpolate(
    [cardProps.swipe],
    (swipe) => `${swipe > 0 ? 100 : 0}`
  );

  const handlers = useSwipeable({
    trackMouse: true,
    delta: 0,
    onSwiping: ({ deltaX }) => {
      if (action.current) return;
      setUistate({
        isSwiping: true,
        angle: deltaX,
      });
    },
    onSwiped: ({ deltaX }) => {
      if (action.current) return;
      if (deltaX < thresholds[0]) {
        action.current = "left";
        setUistate({
          isSwiping: false,
          angle: -300,
        });
      } else if (deltaX > thresholds[1]) {
        action.current = "right";
        setUistate({
          isSwiping: false,
          angle: 300,
        });
      } else {
        setUistate({
          isSwiping: false,
          angle: 0,
        });
      }
    },
  });

  const isPastRight = uiState.angle > thresholds[1];
  const isPastLeft = uiState.angle < thresholds[0];
  const [actionLabel, previewTweet] = useMemo(() => {
    const currentActionPreview = isPastRight
      ? "right"
      : isPastLeft
      ? "left"
      : undefined;
    if (currentActionPreview) {
      const answers = getAnswer(tweet, currentActionPreview, gamestate);
      return [
        actionHints[currentActionPreview],
        answers ? answers[0] : undefined,
      ];
    }

    if (action.current && action.current !== "DONE")
      return ["", getAnswer(tweet, action.current, gamestate)[0]];
    else return [];
  }, [isPastRight, isPastLeft, actionHints, tweet, gamestate]);

  const tweetPreviewProps = useSpring({
    x: previewTweet ? 1 + (action.current ? 2 : 0) : 0,
  });

  const interpolatedHintPosition = cardProps.a.interpolate(
    (a) =>
      `${rotate(a, true)} translateY(${
        actionLabel === undefined ? 0 : Math.min(80, Math.abs(a))
      }px)`
  );

  const cardAppearance = useSpring({
    from: !action.current
      ? { opacity: 0, position: "-100%" }
      : { opacity: 1, position: "0%" },
    to: !action.current
      ? { opacity: 1, position: "0%" }
      : { opacity: 0, position: "0%" },
    onRest: (...params) => {
      if (action.current) {
        if (action.current === "left") left(tweet);
        if (action.current === "right") right(tweet);
        action.current = "DONE";
      }
    },
  });

  const isFirstCard = gamestate.tick === 0;

  return (
    <div {...handlers} className="flex items-end h-full">
      {isFirstCard && (
        <div
          className="absolute w-full h-64 z-50"
          style={{ bottom: "100%" }}
        ></div>
      )}
      <div
        className="relative mx-auto mt-auto text-xl font-extrabold tracking-wide uppercase pointer-events-none"
        style={{
          width: `${(height * 0.95) / 1.395}px`,
          height: `${height * 0.95}px`,
        }}
      >
        <animated.div
          className="w-full h-full"
          style={{
            opacity: cardAppearance.opacity,
            transform: cardAppearance.position.interpolate(
              (v) => `translateY(${v})`
            ),
          }}
        >
          <div className="w-full h-full select-none">
            <animated.div
              style={{
                transform: interpolatedTransform,
                boxShadow: interpolatedShadow,
                zIndex: interpolatedZIndex,
                transformOrigin: "50% 400%",
                willChange: "transform box-shadow",
              }}
              className={classnames(
                "rounded-sm mx-auto h-full w-full flex justify-center items-center uppercase tracking-wide xs:text-xl text-lg tracking-tighter font-extrabold relative overflow-hidden "
              )}
            >
              <animated.div
                className="absolute left-0 w-full font-mono align-bottom"
                style={{
                  top: "-100px",
                  height: "70px",
                  willChange: "transform",
                  transform: interpolatedHintPosition,
                }}
              >
                <div
                  className="absolute bottom-0 h-full m-auto bg-gray-400"
                  style={{
                    height: "300px",
                    left: "-300px",
                    width: "1000px",
                  }}
                ></div>
                <p
                  className={classnames(
                    "absolute bottom-0 w-full text-gray-800 tracking-tighter",
                    {
                      "text-right": uiState.angle < 0,
                    }
                  )}
                >
                  {actionLabel}
                </p>
              </animated.div>

              {/* Hidden top layer to prevent long click / "menu clicking"*/}
              <div className="absolute top-0 left-0 w-full h-full bg-transparent"></div>

              <div
                className="flex flex-col items-center justify-center w-full h-full "
                id="card-content"
              >
                {children}
              </div>
            </animated.div>
          </div>
        </animated.div>
      </div>
      <animated.div
        className="absolute bottom-0 w-full p-4 select-none"
        style={{
          transform: tweetPreviewProps.x.interpolate(
            (x) => `translateY(${(1 - x) * 40 + 60}%)`
          ),
          opacity: tweetPreviewProps.x.interpolate((x) =>
            x < 1 ? x : 1 - (x - 1) / 2
          ),
          zIndex: interpolatedZIndex,
        }}
      >
        {previewTweet && (
          <TweetCard tweet={{ ...previewTweet, type: "answerPreview" }} />
        )}
      </animated.div>
    </div>
  );
};

export default Card;
