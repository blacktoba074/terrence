import React from "react";
import CardContent from "./content";

import GenericCard from "./generic-card";

const Card = ({ left, right, gamestate, height }) => {
  const tweet = gamestate.tweet;
  const actionHints = {
    right: "What!?",
    left: "What?!",
  };

  return (
    <GenericCard
      left={left}
      right={right}
      gamestate={gamestate}
      actionHints={actionHints}
      height={height}
    >
      <div className="w-full h-full bg-citerate-sand-yellow">
        <CardContent
          title="You're fired"
          illustrationPath={tweet.illustrationPath}
          color="#F8F5EE"
          accent="#F8F5EE"
        ></CardContent>
      </div>
    </GenericCard>
  );
};

export default Card;
