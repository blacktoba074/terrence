import React from "react";

import GenericCard from "./generic-card";
import { formatInteger, abbreviateMoney } from "services/format";
import CardContent from "./content";

const Card = ({ left, right, gamestate, height }) => {
  const tweet = gamestate.tweet;
  const actionHints = {
    left: "Keep it",
    right: "Expand",
  };

  return (
    <GenericCard
      left={left}
      right={right}
      gamestate={gamestate}
      actionHints={actionHints}
      height={height}
    >
      <CardContent
        title="Expand the city?"
        illustrationPath={tweet.illustrationPath}
        accent="#CDE1BC"
        color="#ECF4E5"
      >
        New plots: {formatInteger(gamestate.tweet.impact.newPlots)}
        <br />
        Price: {abbreviateMoney(gamestate.tweet.impact.cost)}
      </CardContent>
    </GenericCard>
  );
};

export default Card;
