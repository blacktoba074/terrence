import React from "react";

import GenericCard from "./generic-card";
import CardContent from "./content";

import { abbreviateMoney } from "services/format";
import { getCategoryById } from "config/services";

const Card = ({ left, right, gamestate, height }) => {
  const tweet = gamestate.tweet;
  const { service, category: serviceType } = tweet;
  const serviceDetails = getCategoryById(serviceType);

  const actionHints = {
    left: "Do nothing",
    right: "Buy",
  };

  return (
    <GenericCard
      left={left}
      right={right}
      gamestate={gamestate}
      actionHints={actionHints}
      height={height}
    >
      <CardContent
        title={service.label}
        illustrationPath={service.illustrationPath}
        color={serviceDetails.color}
        accent={serviceDetails.accent}
      >
        <span className="font-thin">Cost:</span>{" "}
        <span className="font-normal">{abbreviateMoney(service.price)}</span>
        <br />
        <span className="font-thin">Maintenance:</span>{" "}
        <span className="font-normal normal-case">
          {abbreviateMoney(service.upkeep)} per citizen
        </span>
        <span className="block font-normal capitalize w-full text-center m-0 p-0">
          {serviceDetails.label}
        </span>
      </CardContent>
    </GenericCard>
  );
};

export default Card;
