import React from "react";

import GenericCard from "./generic-card";
import CardContent from "./content";

import { formatPercent, abbreviateMoney } from "services/format";

const IntroCard = ({ left, right, gamestate, height }) => {
  const tweet = gamestate.tweet;
  const { actionHints } = tweet;

  return (
    <GenericCard
      left={left}
      right={right}
      gamestate={gamestate}
      actionHints={actionHints}
      height={height}
    >
      <CardContent
        title="Build here?"
        illustrationPath={tweet.illustrationPath}
      >
        Payment: {abbreviateMoney(tweet.cost.money)}
        {Boolean(tweet.cost.stocks) && (
          <>
            <br />
            Stocks: {formatPercent(tweet.cost.stocks / 10000)}
          </>
        )}
      </CardContent>
    </GenericCard>
  );
};

export default IntroCard;
