import React from "react";
import CardContent from "./content";

import GenericCard from "./generic-card";
import { abbreviateMoney, formatPercent } from "services/format";

const Card = ({ left, right, gamestate, height }) => {
  const tweet = gamestate.tweet;
  const actionHints = {
    right: "Sell",
    left: "Ignore",
  };

  return (
    <GenericCard
      left={left}
      right={right}
      gamestate={gamestate}
      actionHints={actionHints}
      height={height}
    >
      <CardContent
        title="Sell stocks?"
        color="#E7FBD7"
        accent="#B7E184"
        illustrationPath={tweet.illustrationPath}
      >
        Price:{" "}
        {abbreviateMoney(
          gamestate.tweet.unitStockPrice * (gamestate.tweet.percentage * 10000)
        )}
        <br />
        Stocks: {formatPercent(gamestate.tweet.percentage)}
      </CardContent>
    </GenericCard>
  );
};

export default Card;
