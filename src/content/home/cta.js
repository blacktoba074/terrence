import Link from "components/generic/link";
import React from "react";

/**
 * Determine the mobile operating system.
 * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
 *
 * @returns {String}
 */
export function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent)) {
    return "Windows Phone";
  }

  if (/android/i.test(userAgent)) {
    return "Android";
  }

  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "iOS";
  }

  return "unknown";
}

const CTAButton = ({ children, subtitle, ...props }) => (
  <Link
    {...props}
    className="text-2xl sm:text-3xl md:text-6xl px-8 py-4 bg-citerate-deep-purple flex flex-col justify-center items-center uppercase rounded-full shadow-xl mt-2"
  >
    <div className="-mb-1 md:-mb-2 text-white font-black tracking-wide md:tracking-widest">
      {children}
    </div>
    <div className="text-xs md:text-sm text-white mb-2 font-semibold tracking-wide">
      {subtitle}
    </div>
  </Link>
);

const AndroidLink = () => (
  <Link
    href="https://play.google.com/store/apps/details?id=llc.rients.citeratebuild"
    className="text-center"
  >
    Get it on Android
  </Link>
);
const IOSLink = () => (
  <Link
    href="https://apps.apple.com/us/app/citerate/id1534573293"
    className="text-center"
  >
    Get it on iPhone
  </Link>
);
const WebLink = () => (
  <Link to="/title" className="text-center">
    Play in your browser
  </Link>
);

const CTA = () => {
  const os = getMobileOperatingSystem();
  let links = [];
  let button = undefined;

  if (os === "Android") {
    button = (
      <CTAButton
        href="https://play.google.com/store/apps/details?id=llc.rients.citeratebuild"
        subtitle="on the Play Store"
      >
        Get it now
      </CTAButton>
    );
    links = [<WebLink key="weblink" />, <IOSLink key="ioslink" />];
  } else if (os === "iOS") {
    button = (
      <CTAButton
        href="https://apps.apple.com/us/app/citerate/id1534573293"
        subtitle="on the App Store"
      >
        Get it now
      </CTAButton>
    );
    links = [<WebLink key="weblink" />, <AndroidLink key="androidlink" />];
  } else {
    button = (
      <CTAButton to="title" subtitle="in your browser">
        Play now
      </CTAButton>
    );
    links = [<AndroidLink key="androidlink" />, <IOSLink key="ioslink" />];
  }

  return (
    <div className="flex flex-col flex-wrap content-center justify-center w-full uppercase max-w-lg m-auto mb-8">
      {button}
      <div className="text-white text-sm text-center mt-4 hidden md:block">
        or
      </div>
      <div className=" m-auto flex flex-col text-white hidden md:flex flex-row justify-center w-full underline text-sm">
        {links}
      </div>
    </div>
  );
};

export { CTA };
