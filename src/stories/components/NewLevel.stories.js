import React from "react";

import "assets/main.css";
import LevelReached from "components/newLevel";

export default {
  title: "components/levelReached",
  component: LevelReached,
  argTypes: {
    show: { control: "boolean", defaultValue: false },
  },
};

const commonProps = {
  cityName: "New Pretoria",
};

export const Level1 = (props) => (
  <LevelReached level={1} {...commonProps} {...props} />
);
export const Level2 = (props) => (
  <LevelReached level={2} {...commonProps} {...props} />
);
export const Level3 = (props) => (
  <LevelReached level={3} {...commonProps} {...props} />
);
export const Level4 = (props) => (
  <LevelReached level={4} {...commonProps} {...props} />
);
export const Level5 = (props) => (
  <LevelReached level={5} {...commonProps} {...props} />
);
export const Level6 = (props) => (
  <LevelReached level={6} {...commonProps} {...props} />
);
export const Level7 = (props) => (
  <LevelReached level={7} {...commonProps} {...props} />
);
export const Level8 = (props) => (
  <LevelReached level={8} {...commonProps} {...props} />
);
