import React from "react";
import { storiesOf } from "@storybook/react";
// import { action } from "@storybook/addon-actions";
import EventCard from "components/cards/event";
import events from "config/data/event-definitions.json";

const story = storiesOf("components/cards/event");
const sortedEvents = [...events];
sortedEvents.sort((e1, e2) =>
  e1.infrastructure.localeCompare(e2.infrastructure)
);

sortedEvents.forEach((event) => {
  const gamestate = {
    tweet: { ...event },
  };
  story.add(`${event.infrastructure} - ${event.name}`, () => (
    <EventCard gamestate={gamestate} height={300} />
  ));
});
