import React, { useState } from "react";
import { storiesOf } from "@storybook/react";
// import { action } from "@storybook/addon-actions";
import InfraCard from "components/cards/infra";
import { allServices, withIllustration } from "config/services/tree";

const story = storiesOf("components/cards/infra");
const services = [...allServices];

services.sort((s1, s2) => s1.category.localeCompare(s2.category));

services.forEach((service) => {
  // const { service, category: serviceType } = tweet;
  const gamestate = {
    tweet: { service: withIllustration(service), category: service.category },
  };
  story.add(`${service.category} - ${service.label}`, () => {
    const [key, setKey] = useState(0);
    const onSwipe = () => {
      setKey(key + 1);
    };
    return (
      <InfraCard
        gamestate={gamestate}
        key={key}
        height={300}
        left={onSwipe}
        right={onSwipe}
      />
    );
  });
});
