import React from "react";

import "assets/main.css";
import NewTurn from "components/newTurn";

export default {
  title: "components/NewTurn",
  component: NewTurn,
  argTypes: {
    show: { control: "boolean", defaultValue: false },
    onMessageDisplayed: {
      table: { disable: true },
    },
  },
};

// Minimal gamestate required for component. Must update if the component uses other parts of the state
const gamestateTurn0 = {
  bankBalance: 0,
  turn: 0,
  population: 0,
  cityName: "New Pretoria",
};

const gamestateTurn1 = {
  ...gamestateTurn0,
  bankBalance: 100,
  turn: 1,
  population: 5,
};

const gamestateTurn2 = {
  ...gamestateTurn1,
  bankBalance: 1000,
  turn: 2,
  population: 100,
};

export const FirstTurn = (props) => (
  <NewTurn gamestate={gamestateTurn0} history={[]} {...props} />
);

export const SecondTurn = (props) => (
  <NewTurn gamestate={gamestateTurn1} history={[gamestateTurn0]} {...props} />
);

export const ThirdTurn = (props) => (
  <NewTurn
    gamestate={gamestateTurn2}
    history={[gamestateTurn0, gamestateTurn1]}
    {...props}
  />
);
