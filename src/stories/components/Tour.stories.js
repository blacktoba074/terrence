import React from "react";

import "assets/main.css";
import { TourTutorial } from "screens/Game/tour";

export default {
  title: "components/tour",
  component: TourTutorial,
};

const steps = [
  {
    target: ".root",
    title: "Welcome!",
    content: "This your new office! Let me walk you through.",
    placement: "center",
  },
  {
    target: ".a",
    content:
      "This is the 21st century, you get the requests from the citizens and news from the collaborators directly in your social network feed.",
  },
  {
    target: ".b",
    content:
      "Contracts will appear here. Swipe left to refuse. Swipe right to accept.",
  },
  {
    target: ".c",
    content:
      "Finally this bar contains all the Key Performance Indicator of your city. If you click on the icons, you'll get more detailed information.",
  },
];

export const Tour = (props) => (
  <div className="flex flex-col w-full h-full root">
    <TourTutorial steps={steps} {...props} />
    <div className="h-10 a">Something</div>
    <div className="h-10 b">Something else</div>
    <div className="h-10 c">Something else else</div>
  </div>
);
