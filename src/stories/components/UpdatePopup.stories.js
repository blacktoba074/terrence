import React from "react";

import "assets/main.css";
import { UpdatePopup } from "components/popups/update";
import { action } from "@storybook/addon-actions";

export default {
  title: "components/updatePopup",
  component: UpdatePopup,
  argTypes: {
    serviceWorkerInitialized: { control: "boolean", defaultValue: false },
    serviceWorkerUpdated: { control: "boolean", defaultValue: false },
    reload: { table: { disable: true } },
  },
};

export const Primary = (props) => {
  const reload = action("Reload the Service Worker");
  return <UpdatePopup reload={reload} {...props} />;
};
