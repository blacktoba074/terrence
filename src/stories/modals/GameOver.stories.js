import React from "react";

import "assets/main.css";

import gamestate from "stories/data/sample-data.json";

import GameOverModal from "components/modals/gameover";

export default {
  title: "components/modals/gameover",
  component: GameOverModal,
  argTypes: {
    show: {
      control: "boolean",
      defaultValue: true,
    },
  },
};

const looseData = (looseCondition) => ({
  ...gamestate,
  isGameOver: true,
  isWin: false,
  gameOverStatus: {
    money: looseCondition === "money",
    time: looseCondition === "time",
    happiness: looseCondition === "happiness",
    noGrowth: looseCondition === "noGrowth",
    quit: looseCondition === "quit",
  },
});

export const NoMoney = (props) => (
  <GameOverModal gamestate={looseData("money")} {...props} />
);

export const Time = (props) => (
  <GameOverModal gamestate={looseData("time")} {...props} />
);

export const Happiness = (props) => (
  <GameOverModal gamestate={looseData("happiness")} {...props} />
);

export const NoGrowth = (props) => (
  <GameOverModal gamestate={looseData("noGrowth")} {...props} />
);

export const Quit = (props) => (
  <GameOverModal gamestate={looseData("quit")} {...props} />
);
