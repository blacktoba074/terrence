import React from "react";

import WinModal from "components/modals/win";

import "assets/main.css";

import winGamestate from "../data/sample-win.json";

export default {
  title: "components/modals/win",
  component: WinModal,
  argTypes: {
    show: { control: "boolean", defaultValue: true },
  },
};

export const WinModalPrimary = (props) => (
  <WinModal
    gamestate={winGamestate}
    history={[winGamestate, winGamestate, winGamestate]}
    {...props}
  />
);
