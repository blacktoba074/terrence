import React from "react";

import IntroModal from "components/modals/intro";

import "assets/main.css";

export default {
  title: "components/modals/intro",
  component: IntroModal,
  argTypes: {
    show: { control: "boolean", defaultValue: true },
  },
};

export const Primary = (props) => <IntroModal {...props} />;
