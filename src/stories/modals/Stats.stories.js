import React from "react";

import "assets/main.css";

import gamestate from "stories/data/sample-data.json";
import sampleUIState from "stories/data/sample-ui-state.json";
import { allServices } from "config/services/tree";

import StatsModal from "components/modals/stats";

export default {
  title: "components/modals/stats",
  component: StatsModal,
  argTypes: {
    show: {
      control: "boolean",
      defaultValue: true,
    },
  },
};

const data = ({
  availableServices = [
    "SharedHotspot",
    "Wells",
    "FirstAidKit",
    "IndividualGenerator",
    "UnpavedRoads",
    "FireExtinguishers",
  ],
  ...otherProperties
} = {}) => ({
  ...gamestate,
  availableServices,
  ...otherProperties,
});

const uiState = ({ ...otherProperties }) => ({
  ...sampleUIState,
  ...otherProperties,
});

export const People = (props) => (
  <StatsModal
    gamestate={data()}
    gameUIState={uiState({ statsScreen: "people" })}
    {...props}
  />
);

export const PeopleAllServices = (props) => (
  <StatsModal
    gamestate={data({
      availableServices: allServices.map((s) => s.id),
    })}
    gameUIState={uiState({ statsScreen: "people" })}
    {...props}
  />
);

export const Housing = (props) => (
  <StatsModal
    gamestate={data()}
    gameUIState={uiState({ statsScreen: "housing" })}
    {...props}
  />
);
export const Finance = (props) => (
  <StatsModal
    gamestate={data()}
    gameUIState={uiState({ statsScreen: "finances" })}
    history={[data(), data()]}
    {...props}
  />
);
export const Time = (props) => (
  <StatsModal
    gamestate={data()}
    gameUIState={uiState({ statsScreen: "time" })}
    {...props}
  />
);
