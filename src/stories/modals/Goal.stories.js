import React from "react";
import GoalReachedModal from "components/modals/goal";
import "assets/main.css";
import gamestate from "stories/data/sample-data.json";

export default {
  title: "components/modals/goal",
  component: GoalReachedModal,
  argTypes: {
    show: { control: "boolean", defaultValue: true },
  },
};

export const GoalReachedModalPrimary = (props) => (
  <GoalReachedModal gamestate={gamestate} {...props} />
);
