import React from "react";

import "assets/main.css";

import BillsShower from "components/effects/billshower";
import { action } from "@storybook/addon-actions";

export default {
  title: "components/effects/billshower",
  component: BillsShower,
  argTypes: {
    show: {
      control: "boolean",
      defaultValue: true,
    },
    onRest: {
      table: {
        disable: true,
      },
    },
  },
};

export const Primary = (props) => {
  const onRest = () => {
    action("On rest");
  };
  return <BillsShower {...props} onRestRef={onRest} />;
};
