import React from "react";

import "assets/main.css";

import LoveWall from "components/effects/lovewall";
import { action } from "@storybook/addon-actions";

export default {
  title: "components/effects/lovewall",
  component: LoveWall,
  argTypes: {
    show: {
      control: "boolean",
      defaultValue: true,
    },
    onRest: {
      table: {
        disable: true,
      },
    },
  },
};

export const Primary = (props) => {
  const onRest = () => {
    action("On rest");
  };
  return <LoveWall {...props} onRestRef={onRest} />;
};
