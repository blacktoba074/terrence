import React from "react";

import fs from "fs";
import path from "path";

import { Box, Text, render } from "ink";

import bots from "./bot";
import Runner from "./ui/runner";

import { Command } from "commander";
const program = new Command();

program
  .requiredOption("--bot <bot-name>", "Name of the bot to use for the run")
  .option("--runs <number>", "Number of runs the bot play", 100)
  .option("--turns <number>", "Maximum number of turns the bot can play", 100);

program.parse(process.argv);
const { bot, runs, turns } = program;

if (!Object.keys(bots).includes(program.bot)) {
  console.log(`Invalid bot name. Valid values: ${Object.keys(bots).join(" ")}`);
  console.log("Name provided : ", program.bot);

  process.exit(1);
}

const HeadlessGame = ({ botName = "left", folder }) => {
  return (
    <Box flexDirection="column">
      <Text color="green">
        Running Citerate automatically | Bot : {botName}
      </Text>
      <Runner
        endGameCallback={(endValue) => {
          writeRunJSON(endValue, folder);
        }}
        behavior={bots[botName]}
        nbRun={runs}
        speed={1}
        maxTurns={turns}
      />
    </Box>
  );
};

function getFolderName(botName, i = 1) {
  const folder = path.join(
    "runs",
    `${new Date().toDateString()}`.replace(/ /g, "-"),
    `${botName}-${i}`
  );

  if (fs.existsSync(folder)) return getFolderName(botName, i + 1);

  return folder;
}

function writeRunJSON(end, folder) {
  fs.mkdirSync(folder, { recursive: true });
  const fileName = path.join(folder, `${Date.now()}.json`);

  fs.writeFileSync(fileName, JSON.stringify(end, null, 2));

  console.log(`Output file: ${fileName}`);
}

async function run(botName) {
  const folderName = getFolderName(botName);
  const instance = render(
    <HeadlessGame botName={botName} folder={folderName} />
  );
  await instance.waitUntilExit();
}

run(bot);
