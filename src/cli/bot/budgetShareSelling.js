// Minimum happiness for expansion
const happinessThreshold = 45;

// Bootstrap budget bot
// No VC
// Buy if bankBalance > investment cost
export default ({ state }) => {
  const { tweet, tick, turn, bankBalance, happiness } = state;
  if (tweet.type === "infra") {
    const investment = tweet.service.price;
    return investment > bankBalance;
  }
  if (tweet.type === "plot") {
    if (turn < 2) return false;
    return happiness < happinessThreshold;
  }
  if (tweet.type === "intro") {
    if (tick === 0) return true;
    if (tick === 1) return false;
  }
  if (tweet.type === "stocks" && turn > 30) {
    return false;
  }
  return true;
};
