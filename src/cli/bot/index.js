import random from "./random";
import left from "./onlyLeft";
import budget from "./budgetSmart";
import vc from "./budgetShareSelling";

export default {
  random,
  budget,
  left,
  vc,
};
