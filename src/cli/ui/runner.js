import React, { useEffect, useRef } from "react";

import useGameState from "../../hooks/gamestate";
import Log from "./log";

const Runner = ({
  maxTurns = 100,
  endGameCallback,
  behavior,
  speed = 1,
  nbRun,
}) => {
  const currentRun = useRef(1);
  const gamestateAPI = useGameState();

  const doLeftMove = behavior(gamestateAPI);

  useEffect(() => {
    const {
      left,
      right,
      state: gamestate,
      history,
      isGameOver,
      isWin,
      reset,
    } = gamestateAPI;

    const nextRun = () => {
      if (currentRun.current < nbRun) {
        currentRun.current = currentRun.current + 1;
        reset();
      }
    };

    if (gamestate.turn >= maxTurns) {
      endGameCallback({
        gamestate,
        history,
        end: "unfinished",
      });
      nextRun();
    } else if (isGameOver) {
      endGameCallback({
        gamestate,
        history,
        end: "gameover",
      });
      nextRun();
    } else if (isWin) {
      endGameCallback({
        gamestate,
        history,
        end: "win",
      });
      nextRun();
    } else {
      setTimeout(() => {
        if (doLeftMove) left();
        else right();
      }, speed);
    }
  }, [gamestateAPI, doLeftMove, endGameCallback, speed, maxTurns, nbRun]);

  return (
    <Log
      data={gamestateAPI.history}
      format={(s) =>
        `${s.turn}(${s.tick}): ${s.tweet.message} | Next move: ${
          doLeftMove ? "left" : "right"
        }`
      }
    />
  );
};

export default Runner;
