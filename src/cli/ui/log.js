import React from "react";

import { Text, Box } from "ink";

const Log = ({ data, format }) => {
  const dataCopy = [...data];
  const lines = dataCopy
    .reverse() //    |
    .slice(0, 10) // |=> this for taking the last ten
    .map(format) // format should return strings
    .reverse()
    .map((l, i) => {
      return <Text key={i}>{l}</Text>;
    });

  return (
    <Box flexDirection="column" minHeight={10}>
      {lines}
    </Box>
  );
};

export default Log;
