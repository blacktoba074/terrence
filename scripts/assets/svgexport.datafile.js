const fs = require("fs");
const path = require("path");

const cwd = process.cwd();

const INFRA_CARDS_SOURCE_FOLDER = "resources/other/card-icons";
const INFRA_CARDS_OUTPUT_FOLDER = "public/cards";

const EVENT_CARDS_SOURCE_FOLDER = "resources/other/events";
const EVENT_CARDS_OUTPUT_FOLDER = "public/events";

const LEVEL_CARDS_SOURCE_FOLDER = "resources/other/levels";
const LEVEL_CARDS_OUTPUT_FOLDER = "public/levels";

const OTHER_CARDS_SOURCE_FOLDER = "resources/other/otherscreens";
const OTHER_CARDS_OUTPUT_FOLDER = "public/other-cards";

const AVATARS_SOURCE_FOLDER = "resources/other/avatars";
const AVATARS_OUTPUT_FOLDER = "public/avatars";

const CHARACTERS_SOURCE_FOLDER = "resources/other/avatars-characters";
const CHARACTERS_OUTPUT_FOLDER = "public/avatars";

// Infrastructure cards
const infraFiles = fs.readdirSync(INFRA_CARDS_SOURCE_FOLDER);
const assetData = [];

const infraCardsDatafile = infraFiles
  .filter((f) => !f.startsWith("."))
  .map((filename) => {
    const contentName = filename.replace(/C_[0-9]{2}_(\w+)_[0-9]+.svg/, "$1");

    const [type, building] = contentName.split("_");
    const exportName = contentName + ".png";

    assetData.push({
      type,
      building,
      file: exportName,
    });

    return {
      input: [path.join(cwd, INFRA_CARDS_SOURCE_FOLDER, filename)],
      output: [
        [path.join(cwd, INFRA_CARDS_OUTPUT_FOLDER, exportName), "1024:"],
      ],
    };
  });

// Level illustrations
const levelFiles = fs.readdirSync(LEVEL_CARDS_SOURCE_FOLDER);
const levelDatafile = levelFiles
  .filter((f) => !f.startsWith("."))
  .map((filename) => {
    const contentName = filename.replace(/C_[0-9]{2}_(\w+)_.*.svg/, "$1");
    const exportName = contentName + ".png";

    return {
      input: [path.join(cwd, LEVEL_CARDS_SOURCE_FOLDER, filename)],
      output: [
        [path.join(cwd, LEVEL_CARDS_OUTPUT_FOLDER, exportName), "1024:"],
      ],
    };
  });

// Event card illustrations
const eventFiles = fs.readdirSync(EVENT_CARDS_SOURCE_FOLDER);
const eventCardsDatafile = eventFiles
  .filter((f) => !f.startsWith("."))
  .map((filename) => {
    const contentName = filename.replace(/(.*).svg/, "$1");
    const exportName = contentName + ".png";

    return {
      input: [path.join(cwd, EVENT_CARDS_SOURCE_FOLDER, filename)],
      output: [
        [path.join(cwd, EVENT_CARDS_OUTPUT_FOLDER, exportName), "1024:"],
      ],
    };
  });

// Misc. other card illustrations
const otherFiles = fs.readdirSync(OTHER_CARDS_SOURCE_FOLDER);
const otherCardsDatafile = otherFiles
  .filter((f) => !f.startsWith("."))
  .map((filename) => {
    const contentName = filename.replace(
      /C_[0-9]{2}-*[a-zA-Z]*_(\w+.*).svg/,
      "$1"
    );
    const exportName = contentName + ".png";

    return {
      input: [path.join(cwd, OTHER_CARDS_SOURCE_FOLDER, filename)],
      output: [
        [path.join(cwd, OTHER_CARDS_OUTPUT_FOLDER, exportName), "1024:"],
      ],
    };
  });

// avatar icons
const avatarFiles = fs.readdirSync(AVATARS_SOURCE_FOLDER);

let currentAvatarId = 1;
const avatarsDatafile = avatarFiles
  .filter((f) => !f.startsWith("."))
  .map((filename) => {
    const exportName = `avatar_${currentAvatarId}.png`;
    currentAvatarId += 1;

    return {
      input: [path.join(cwd, AVATARS_SOURCE_FOLDER, filename)],
      output: [[path.join(cwd, AVATARS_OUTPUT_FOLDER, exportName)]],
    };
  });

// character icons
const charactersFiles = fs.readdirSync(CHARACTERS_SOURCE_FOLDER);

const charactersDatafile = charactersFiles
  .filter((f) => !f.startsWith("."))
  .map((filename) => {
    const exportName = filename.replace(/C_[0-9]{2}_(.+).svg/, "$1.png");

    return {
      input: [path.join(cwd, CHARACTERS_SOURCE_FOLDER, filename)],
      output: [[path.join(cwd, CHARACTERS_OUTPUT_FOLDER, exportName)]],
    };
  });

// Save the generated filenames in the mapping file for the game
try {
  fs.writeFileSync(
    path.join(cwd, "src/config/data/infra-card-illustrations.json"),
    JSON.stringify(assetData, null, 2),
    {
      flag: "w+",
    }
  );
} catch (e) {
  console.log(e);
}

// App assets output
const splashDataFile = [
  // Splash screen
  {
    input: [
      path.join(cwd, "resources/other/logotype/Citerate_SplashScreen-01.svg"),
    ],
    output: [[path.join(cwd, "resources/splash.png"), "pad", "2732:2732"]],
  },
  // App icon
  {
    input: [path.join(cwd, "resources/other/logotype/C_06_AppIcon.svg")],
    output: [[path.join(cwd, "resources/icon.png"), "1024:1024"]],
  },
];

const datafile = [
  ...infraCardsDatafile,
  ...otherCardsDatafile,
  ...avatarsDatafile,
  ...charactersDatafile,
  ...splashDataFile,
  ...eventCardsDatafile,
  ...levelDatafile,
];

module.exports = datafile;
