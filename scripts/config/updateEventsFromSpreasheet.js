const parse = require("csv-parse/lib/sync");
const fetch = require("node-fetch");
const fs = require("fs");

(async function () {
  const response = await fetch(
    "https://docs.google.com/spreadsheets/d/e/2PACX-1vS05owMZK2QIVtrvLeAgBwjD9rty84hmUV6QNe8m8JTaQj4B6_ob8nt9lmTLZE9z7WFUux5FltockXf/pub?gid=1065800353&single=true&output=csv"
  );

  const data = await response.text();

  const events = parse(data, {
    columns: true,
    skip_empty_lines: true,
    cast: (value) => {
      const parsed = parseFloat(value);
      if (!Number.isNaN(parsed)) return parsed;
      return value;
    },
    on_record: (record) => {
      const keys = Object.keys(record);
      const nestedKeys = keys.filter((k) => k.indexOf(".") >= 0);
      nestedKeys.forEach((k) => {
        const [root, subKey, ...rest] = k.split(".");
        if (!record[root]) {
          record[root] = {};
        }
        record[root][subKey] = record[k];
        delete record[k];
      });

      return record;
    },
  });

  fs.writeFileSync(
    "src/config/data/event-definitions.json",
    JSON.stringify(events, null, 2)
  );
})();
