const parse = require("csv-parse/lib/sync");
const fetch = require("node-fetch");
const fs = require("fs");

(async function () {
  const plotsTweetsConfig = await fetch(
    "https://docs.google.com/spreadsheets/d/e/2PACX-1vS05owMZK2QIVtrvLeAgBwjD9rty84hmUV6QNe8m8JTaQj4B6_ob8nt9lmTLZE9z7WFUux5FltockXf/pub?gid=1256979165&single=true&output=csv"
  );

  const plotsLevelData = parse(await plotsTweetsConfig.text(), {
    columns: (header) =>
      header.map((column) => column.toLowerCase().replace(" ", "_")),
    cast: (value) => {
      const parsed = parseFloat(value);
      if (!Number.isNaN(parsed)) return parsed;

      return value;
    },
  });

  fs.writeFileSync(
    "src/config/data/plots_tweets.json",
    JSON.stringify(plotsLevelData, null, 2)
  );
})();
