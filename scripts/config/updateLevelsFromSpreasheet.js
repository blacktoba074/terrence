const parse = require("csv-parse/lib/sync");
const fetch = require("node-fetch");
const fs = require("fs");

(async function () {
  const responseLevels = await fetch(
    "https://docs.google.com/spreadsheets/d/e/2PACX-1vS05owMZK2QIVtrvLeAgBwjD9rty84hmUV6QNe8m8JTaQj4B6_ob8nt9lmTLZE9z7WFUux5FltockXf/pub?gid=351731249&single=true&output=csv"
  );

  const responseInfra = await fetch(
    "https://docs.google.com/spreadsheets/d/e/2PACX-1vS05owMZK2QIVtrvLeAgBwjD9rty84hmUV6QNe8m8JTaQj4B6_ob8nt9lmTLZE9z7WFUux5FltockXf/pub?gid=0&single=true&output=csv"
  );

  const infraData = new Map(
    parse(await responseInfra.text(), {
      columns: true,
      cast: (value) => {
        const parsed = parseFloat(value);
        if (!Number.isNaN(parsed)) return parsed;

        return value;
      },
    }).reduce(
      (infrastructures, row) => [
        ...infrastructures,
        [
          row["Equipment name"],
          {
            id: row["id"],
            price: row["Price"],
            upkeep: row["Upkeep"],
          },
        ],
      ],
      []
    )
  );

  const dataLevels = await responseLevels.text();
  const events = parse(dataLevels, {
    columns: (header) => header.map((c) => (c === "" ? null : c)),
    skip_empty_lines: true,
    cast: (value) => {
      const parsed = parseFloat(value);
      if (!Number.isNaN(parsed)) return parsed;
      return value;
    },
    on_record: (record) => {
      const keys = Object.keys(record);
      return keys
        .filter((k) => record[k] !== "" && k !== "Level")
        .map((k) => {
          const label = record[k];
          if (!infraData.has(label))
            console.error("Missing infrastructure data for " + label);
          const infrastructure = infraData.get(label);
          return {
            category: k,
            label: record[k],
            ...infrastructure, // contains price, upkeep and id
          };
        });
    },
  });

  fs.writeFileSync(
    "src/config/data/level-infra-card-definitions.json",
    JSON.stringify(events, null, 2)
  );
})();
